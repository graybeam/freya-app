defmodule Rs.League.Division do
  use Ecto.Schema
  import Ecto.Changeset

  schema "divisions" do
    field :title, :string
    field :start_date, :date
    field :end_date, :date

    has_one :division_type, Rs.League.DivisionType
    has_many :registration, Rs.Organization.League.Registration

    timestamps()
  end

  @doc false
  def changeset(division, attrs) do
    division
    |> cast(attrs, [:title, :start_date, :end_date])
    |> validate_required([:title])
  end
end