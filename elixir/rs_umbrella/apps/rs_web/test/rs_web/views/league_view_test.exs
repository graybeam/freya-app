defmodule RsWeb.LeagueViewTest do
  use RsWeb.ConnCase, async: true
  import Phoenix.View

  test "renders index.html", %{conn: conn} do
    leagues = [
      %RsWeb.Organization.League{id: "1", title: "dogs"},
      %RsWeb.Organization.League{id: "2", title: "cats"}
    ]

    content = render_to_string(RsWeb.LeagueView, "index.html", conn: conn, leagues: leagues)

    assert String.contains?(content, "Listing Leagues")

    for league <- leagues do
      assert String.contains?(content, league.title)
    end
  end

  test "renders new.html", %{conn: conn} do
    owner = %Rs.Accounts.User{}
    changeset = RsWeb.Organization.change_league(owner, %RsWeb.Organization.League{})
    categories = [%RsWeb.Organization.Category{id: 123, name: "cats"}]

    content =
      render_to_string(RsWeb.LeagueView, "new.html",
        conn: conn,
        changeset: changeset,
        categories: categories
      )

    assert String.contains?(content, "New League")
  end
end
