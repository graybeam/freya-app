## Problem Space
- League Registration domain
- Team Registration domain
- Player Registration domain
- Admin domain

## Solution Space
- League Registration context
- Team Registration context
- Player Registration context
- Admin context
