defmodule RsWeb.League.Contact do
  alias __MODULE__
  defstruct first_name: nil
  def new(), do: {:ok, %Contact{}}

end
