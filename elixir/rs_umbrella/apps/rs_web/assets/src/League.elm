module LeaguePage exposing (Internals, League(..), main)

import Browser
import Contact
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode
import Json.Encode as Encode


type League
    = League Internals


type alias Internals =
    { name : String
    , location : String
    , url : String
    , slug : String
    , contacts : List Contact.Contact
    }


initLeague =
    { name = "NEORH", location = "Central Akron", url = "http://www.espn.com", slug = "slugemail", contacts = [] }


main =
    Browser.sandbox { init = 0, update = update, view = view }


type Msg
    = Increment
    | Decrement


update msg model =
    case msg of
        Increment ->
            model + 1

        Decrement ->
            model - 1


view model =
    div []
        [ button [ onClick Decrement ] [ text "-" ]
        , div [] [ text (String.fromInt model) ]
        , button [ onClick Increment ] [ text "+" ]
        , viewLeagueInfoBox initLeague
        ]


viewLeagueInfoBox : Internals -> Html Msg
viewLeagueInfoBox league =
    div [] [ text "League Info Box" ]
