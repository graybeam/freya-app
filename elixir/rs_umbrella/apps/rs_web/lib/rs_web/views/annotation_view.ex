defmodule RsWeb.AnnotationView do
  use RsWeb, :view

  def render("annotation.json", %{annotation: annotation}) do
    %{
      id: annotation.id,
      body: annotation.body,
      at: annotation.at,
      user: render_one(annotation.user, RsWeb.UserView, "user.json")
    }
  end
end
