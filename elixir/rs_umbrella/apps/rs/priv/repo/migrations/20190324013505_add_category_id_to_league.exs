defmodule RsWeb.Repo.Migrations.AddCategoryIdToLeague do
  use Ecto.Migration

  def change do
    alter table(:leagues) do
      add :category_id, references(:categories)
    end
  end
end
