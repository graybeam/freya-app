defmodule RsWeb.PageController do
  use RsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
