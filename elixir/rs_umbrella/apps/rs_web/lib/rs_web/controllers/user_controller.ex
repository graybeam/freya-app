defmodule RsWeb.UserController do
  use RsWeb, :controller

  alias Rs.Accounts
  alias Rs.Accounts.User
  alias RsWeb.Email
  alias RsWeb.Mailer
  require Logger

  plug :authenticate_user when action in [:index, :show]

  def new(conn, _params) do
    changeset = Accounts.change_registration(%User{}, %{})
    render(conn, "new.html", changeset: changeset, registered_user: false)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->

        Email.registration_email(user) |> Mailer.deliver_later()
        Logger.info("creating user with this connection:  #{inspect(conn)}")
        conn
        |> RsWeb.Auth.login(user)
        |> put_flash(:info, "#{user.name} created!")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user(id)
    render(conn, "show.html", user: user)
  end

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end
end
