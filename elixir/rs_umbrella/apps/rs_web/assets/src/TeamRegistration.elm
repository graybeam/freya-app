port module TeamRegistration exposing (Model, Msg(..), Progress(..), Team, encodeLeadersDict, encodeTeam, initialLeadersDict, initialLocation, initialModel, main, statusBar, update, updateCurrentLeaderEmail, updateCurrentLeaderName, updateCurrentLeaderPhone, updateCurrentLeaderType, updateLeaderDict, updateLocation, updateLocationCity, updateLocationLine1, updateLocationLine2, updateLocationPostalCode, updateLocationState, view, viewAddressForm, viewBasicInfoForm, viewFooter, viewHeader, viewInfoBox, viewJumbotron, viewLeaderForm, viewLeaderInfo, viewLeaderTypeChooser, viewLeadersInfo, viewProgressBar)

import Browser
import Dict exposing (Dict)
import Html exposing (Attribute, Html, a, button, div, footer, form, h1, h5, header, img, input, label, li, nav, option, p, pre, select, text, ul)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Decode exposing (Decoder, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Enc
import Leader as L
import Location as Loc
import String


maxLeaders =
    3


phoenixApiUrl =
    "http://localhost:4000/api/v1/teams"


postmanMockApiUrl =
    "https://17b6920b-8645-49c5-b8db-def5d783296a.mock.pstmn.io/api/v1/teams"


type alias FeedbackMsg =
    { name : String, description : String }


type alias Team =
    { name : String
    , leaders : List L.Leader
    , location : Loc.Location
    }


type Progress
    = EnteringBasicInfo
    | EnteringLeaderInfo
    | FinishedRegistration
    | NotStartedRegistration


type ReadyToSubmit
    = Complete
    | NotComplete



--type LeaderDict
--    = Dict Int Leader


type alias Model =
    { name : String
    , location : Loc.Location
    , leaders : Dict Int L.Leader
    , progress : Progress
    , readyToSubmit : ReadyToSubmit
    , currentLeader : L.Leader
    , sharable_url : String
    , userToken : String
    }


type alias Submission =
    { status : String
    , message : String
    , id : String
    , request_id : String
    , sharable_url : String
    }


initialLeadersDict : Dict Int L.Leader
initialLeadersDict =
    Dict.fromList [ ( 0, L.initialLeader ) ]


initialLocation : Loc.Location
initialLocation =
    Loc.initialLocation


initialModel : String -> Model
initialModel userToken =
    { leaders = Dict.empty --Dict.fromList [ ( 0, { id = 0, name = "", email = "", phone = "", position = Commissioner } ) ]
    , location = initialLocation
    , name = ""
    , progress = NotStartedRegistration
    , readyToSubmit = NotComplete
    , currentLeader = L.initialLeader
    , userToken = userToken
    , sharable_url = ""
    }


type Msg
    = ClickedLeaderType L.LeaderType
    | SaveBasicInfo
    | SaveCurrentLeader Int
    | UpdateTeamName String
    | UpdateLocationLine1 String
    | UpdateLocationLine2 String
    | UpdateLocationCity String
    | UpdateLocationState String
    | UpdateLocationPostalCode String
    | UpdateCurrentLeaderName String
    | UpdateCurrentLeaderEmail String
    | UpdateCurrentLeaderPhone String
    | StartRegistration
    | LoadLeader Int
    | BackToBasicInfo
    | ReceivedInfo (Result Http.Error Submission)
    | SubmitRegistration


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SaveBasicInfo ->
            ( { model | progress = EnteringLeaderInfo }, Cmd.none )

        ClickedLeaderType leaderType ->
            ( updateCurrentLeaderType leaderType model, Cmd.none )

        UpdateTeamName name ->
            ( { model | name = name }, Cmd.none )

        UpdateLocationLine1 line1 ->
            ( updateLocationLine1 line1 model, Cmd.none )

        UpdateLocationLine2 line2 ->
            ( updateLocationLine2 line2 model, Cmd.none )

        UpdateLocationCity city ->
            ( updateLocationCity city model, Cmd.none )

        UpdateLocationState state ->
            ( updateLocationState state model, Cmd.none )

        UpdateLocationPostalCode postal_code ->
            ( updateLocationPostalCode postal_code model, Cmd.none )

        UpdateCurrentLeaderName name ->
            ( updateCurrentLeaderName name model, Cmd.none )

        UpdateCurrentLeaderEmail email ->
            ( updateCurrentLeaderEmail email model, Cmd.none )

        UpdateCurrentLeaderPhone phone ->
            ( updateCurrentLeaderPhone phone model, Cmd.none )

        StartRegistration ->
            ( { model | progress = EnteringBasicInfo }, Cmd.none )

        SaveCurrentLeader index ->
            ( updateLeaderDict index model, Cmd.none )

        LoadLeader index ->
            ( setLeaderToCurrent (getLeader index model) model, Cmd.none )

        BackToBasicInfo ->
            ( { model | progress = EnteringBasicInfo }, Cmd.none )

        ReceivedInfo (Ok submission) ->
            ( { model | progress = FinishedRegistration, sharable_url = submission.sharable_url }, Cmd.none )

        ReceivedInfo (Err httpError) ->
            case httpError of
                Http.BadBody string ->
                    ( { model | name = string }, Cmd.none )

                _ ->
                    ( { model | name = "Some other Error" }, Cmd.none )

        SubmitRegistration ->
            ( model, sendItems model )


main : Program String Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \model -> Sub.none
        }


init : String -> ( Model, Cmd Msg )
init userToken =
    ( initialModel userToken, Cmd.none )



-- use array for this from feldman example , then save the leader form.
-- takes a list of leaders and if there is a value ---
-- COMMANDS
--


zapierDecoder : Decoder Submission
zapierDecoder =
    succeed Submission
        |> required "status" string
        |> required "message" string
        |> required "id" string
        |> required "request_id" string
        |> required "shareable_url" string


sendItems : Model -> Cmd Msg
sendItems model =
    Http.request
        { method = "POST", headers = [ Http.header "Authorization" model.userToken ], timeout = Nothing, tracker = Nothing, url = phoenixApiUrl, body = Http.stringBody "application/json" (Enc.encode 3 (encodeTeamTeam model)), expect = Http.expectJson ReceivedInfo zapierDecoder }


jsonBody model =
    Enc.encode 3 (encodeTeamTeam model)



-- ENODING --


encodeTeamTeam model =
    Enc.object [ ( "team", encodeTeam model ) ]


encodeTeam : Model -> Enc.Value
encodeTeam model =
    Enc.object
        [ ( "name", Enc.string model.name )
        , ( "status", Enc.string "URL" ) -- encodeLocation model.location )
        , ( "contacts", encodeLeadersDict model.leaders )
        , ( "location", encodeLocation model.location )
        , ( "url", Enc.string "URL" )
        , ( "description", Enc.string "DESCRIPTION" )
        ]


encodeLocation : Loc.Location -> Enc.Value
encodeLocation location =
    Enc.object
        [ ( "address_line_1", Enc.string location.line1 )
        , ( "address_line_2", Enc.string location.line2 )
        , ( "city", Enc.string location.city )
        , ( "state", Enc.string location.state )
        , ( "postalCode", Enc.string location.postalCode )
        , ( "country", Enc.string "US" ) -- TODO add country option to the form and logic
        ]



--encodeLeadersDict : Dict Int Leader -> List ( String, Enc.Value )


encodeLeadersDict leaders =
    Enc.list L.encode (leaderList leaders)


leaderList leaders =
    Dict.values leaders


saveCurrentLeader : Model -> L.Leader -> L.Leader
saveCurrentLeader model leader =
    { leader
        | name = model.currentLeader.name
        , email = model.currentLeader.email
        , phone = model.currentLeader.phone
        , position = model.currentLeader.position
    }


checkRegistration : Model -> Model
checkRegistration model =
    case numberOfLeaders model > 0 of
        True ->
            { model | readyToSubmit = Complete }

        False ->
            model


numberOfLeaders : Model -> Int
numberOfLeaders model =
    Dict.size model.leaders


updateLeaderDict : Int -> Model -> Model
updateLeaderDict index model =
    case okToSubmitLeaderForm model of
        True ->
            case Dict.member index model.leaders of
                True ->
                    { model | leaders = Dict.update index (Maybe.map (\leader -> saveCurrentLeader model leader)) model.leaders }
                        |> setLeaderToCurrent { id = numberOfLeaders model, name = "", email = "", phone = "", position = L.Other }
                        |> checkRegistration

                False ->
                    if numberOfLeaders model < maxLeaders then
                        { model | leaders = Dict.insert (numberOfLeaders model) model.currentLeader model.leaders }
                            |> setLeaderToCurrent { id = numberOfLeaders model + 1, name = "", email = "", phone = "", position = L.Other }
                            |> checkRegistration

                    else
                        model

        False ->
            model


getLeader : Int -> Model -> L.Leader
getLeader index model =
    case Dict.get index model.leaders of
        Just off ->
            off

        Nothing ->
            L.initialLeader


setLeaderToCurrent : L.Leader -> Model -> Model
setLeaderToCurrent leader model =
    { model | currentLeader = leader }



-- updates one aspect of the model at a time


updateLocation : String -> Model -> Model
updateLocation country ({ location } as model) =
    { model | location = { location | country = country } }


updateLocationLine1 : String -> Model -> Model
updateLocationLine1 line1 ({ location } as model) =
    { model | location = { location | line1 = line1 } }


updateLocationLine2 : String -> Model -> Model
updateLocationLine2 line2 ({ location } as model) =
    { model | location = { location | line2 = line2 } }


updateLocationCity : String -> Model -> Model
updateLocationCity city ({ location } as model) =
    { model | location = { location | city = city } }


updateLocationState : String -> Model -> Model
updateLocationState state ({ location } as model) =
    { model | location = { location | state = state } }


updateLocationPostalCode : String -> Model -> Model
updateLocationPostalCode postal_code ({ location } as model) =
    { model | location = { location | postalCode = postal_code } }


updateCurrentLeaderName name ({ currentLeader } as model) =
    { model | currentLeader = { currentLeader | name = name } }


updateCurrentLeaderEmail email ({ currentLeader } as model) =
    { model | currentLeader = { currentLeader | email = email } }


updateCurrentLeaderPhone phone ({ currentLeader } as model) =
    { model | currentLeader = { currentLeader | phone = phone } }


updateCurrentLeaderType : L.LeaderType -> Model -> Model
updateCurrentLeaderType leaderType ({ currentLeader } as model) =
    { model | currentLeader = { currentLeader | position = leaderType } }


viewLeaderTypeChooser : L.LeaderType -> Html Msg
viewLeaderTypeChooser leaderType =
    div [ class "form-check" ]
        [ input [ type_ "radio", name "leaderType", class "form-check-input", onClick (ClickedLeaderType leaderType) ] []
        , label [ class "form-check-label" ] [ text (L.leaderTypeToString leaderType) ]
        ]


viewHeader =
    div []
        [ header [ class "blog-header py-3" ]
            [ div [ class "row flex-nowrap justify-content-between align-items-center" ]
                [ div [ class "col-4 pt-1" ]
                    [-- a [ class "text-muted" ] [ text "Subscribe" ]
                    ]
                , div [ class "col-4 text-center" ]
                    [ a [ class "blog-header-logo text-dark" ] [ text "Registration System" ]
                    ]
                , div [ class "col-4 d-flex justify-content-end align-items-center" ]
                    [ a [ class "btn btn-sm btn-outline-secondary" ] [ text "Log in" ]
                    ]
                ]
            ]
        , div [ class "nav-scroller py-1 mb-2" ]
            [ nav [ class "nav d-flex justify-content-between" ]
                [ a [ class "p-2 text-muted" ] [ text "Team" ]
                , a [ class "p-2 text-muted" ] [ text "Team" ]
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []

                --, a [ class "p-2 text-muted" ] [ text "Admin" ]
                ]
            ]
        ]


viewJumbotron model =
    case model.progress of
        NotStartedRegistration ->
            div [ classList [ ( "jumbotron p-3 p-md-5 text-white rounded bg-dark row", True ) ], hidden False ]
                [ div [ class "col-md-6 px-0" ]
                    [ h1 [ class "display-4 font-italic" ] [ text "Team Registration" ]
                    , p [ class "form-group" ]
                        [ div [ class "btn btn-secondary m-2" ]
                            [ a [ class "text-light", href "http://www.royhobbs.com/membership-and-insurance/" ] [ text "More Infl..." ] ]
                        , button [ class "btn btn-primary m-2", onClick StartRegistration ] [ text "Start" ]
                        ]
                    ]
                ]

        _ ->
            text ""


viewProgressBar : Model -> Html Msg
viewProgressBar model =
    div [ class "row justify-content-md-center mb-5" ]
        [ statusBar model.progress
        ]



-- check the progress of the model, if model.progress == EnteringBasicInfo, then set the first button class to "btn-primary"


statusBar : Progress -> Html Msg
statusBar progress =
    case progress of
        EnteringBasicInfo ->
            div [ class "btn-group btn-group-lg col-md-auto" ]
                [ button [ class "btn btn-primary" ]
                    [ text "Basic Information"
                    ]
                , button [ class "btn btn-secondary" ]
                    [ text "Leaders"
                    ]
                , button [ class "btn btn-secondary" ]
                    [ text "Submit"
                    ]
                ]

        EnteringLeaderInfo ->
            div [ class "btn-group btn-group-lg col-md-auto" ]
                [ button [ class "btn btn-secondary" ]
                    [ text "Basic Information"
                    ]
                , button [ class "btn btn-primary" ]
                    [ text "Leaders"
                    ]
                , button [ class "btn btn-secondary" ]
                    [ text "Submit"
                    ]
                ]

        FinishedRegistration ->
            div [ class "btn-group btn-group-lg col-md-auto" ]
                [ button [ class "btn btn-secondary" ]
                    [ text "Basic Information"
                    ]
                , button [ class "btn btn-secondary" ]
                    [ text "Leaders"
                    ]
                , button [ class "btn btn-primary" ]
                    [ text "Complete"
                    ]
                ]

        NotStartedRegistration ->
            text ""


viewAddressForm : Model -> Html Msg
viewAddressForm model =
    div []
        [ div [ class "form-group" ]
            [ label [ for "inputAddress" ] [ text "Address" ]
            , input [ type_ "text", class "form-control", id "inputAddress", placeholder "1234 Main St.", value model.location.line1, onInput UpdateLocationLine1 ] []
            ]
        , div [ class "form-group" ]
            [ label [ for "inputAddress2" ] [ text "Address 2" ]
            , input [ type_ "text", class "form-control", id "inputAddress2", placeholder "Apartment, Studio, or Floor", value model.location.line2, onInput UpdateLocationLine2 ] []
            ]
        , div [ class "form-row" ]
            [ div [ class "form-group col-md-6" ]
                [ label [ for "inputCity" ] [ text "City" ]
                , input [ type_ "text", class "form-control", id "inputCity", placeholder "City", value model.location.city, onInput UpdateLocationCity ] []
                ]
            , div [ class "form-group col-md-4" ]
                [ label [ for "inputState" ] [ text "State" ]
                , input [ type_ "text", class "form-control", id "inputState", placeholder "State", value model.location.state, onInput UpdateLocationState ] []
                ]
            , div [ class "form-group col-md-2" ]
                [ label [ for "inputPostalCode" ] [ text "Zip" ]
                , input [ type_ "text", class "form-control", id "inputPostalCode", placeholder "Postal Code", value model.location.postalCode, onInput UpdateLocationPostalCode ] []
                ]
            ]
        ]


viewBasicInfoForm : Model -> Html Msg
viewBasicInfoForm model =
    case model.progress of
        EnteringBasicInfo ->
            Html.form [ class "border rounded p-3 mb-3 ml-3 col-md-6", onSubmit SaveBasicInfo ]
                [ div [ class "form-row" ]
                    [ div [ class "form-group col-md-6" ]
                        [ label [ for "inputTeamName" ] [ text "Team Name" ]
                        , input [ type_ "text", classList [ ( "form-control", True ) ], placeholder "Team Name", id "inputTeamName_", value model.name, onInput UpdateTeamName ] []
                        ]
                    ]
                , viewAddressForm model
                , button [ class "btn btn-link" ] [ text "Cancel" ]
                , button [ disabled False, class "btn btn-primary", type_ "submit" ] [ text "Next" ]
                ]

        _ ->
            text ""


viewLeaderForm : Model -> Html Msg
viewLeaderForm model =
    case model.progress of
        EnteringLeaderInfo ->
            Html.form [ class "border rounded p-3 mb-3 ml-3 col-md-6", onSubmit (SaveCurrentLeader model.currentLeader.id) ]
                -- onSubmit]
                [ div [ class "form-group" ]
                    [ label [ for "fullNameInput" ] [ text "Full Name" ]
                    , input [ type_ "text", class "form-control", placeholder "Full Name", id "fullNameInput", value model.currentLeader.name, onInput UpdateCurrentLeaderName ] []
                    ]
                , div [ class "form-group" ]
                    [ label [ for "emailInput" ] [ text "Email" ]
                    , input [ type_ "email", class "form-control", placeholder "Email", id "emailInput", value model.currentLeader.email, onInput UpdateCurrentLeaderEmail ] []
                    ]
                , div [ class "form-group" ]
                    [ label [ for "phoneInput" ] [ text "Phone" ]
                    , input [ type_ "text", class "form-control", placeholder "Mobile Phone Number", id "phoneInput", value model.currentLeader.phone, onInput UpdateCurrentLeaderPhone ] []
                    ]
                , div [ class "form-group" ]
                    [ div [ id "choose-leader-type", class "form-check" ]
                        (List.map viewLeaderTypeChooser [ L.Secretary, L.Other, L.Manager, L.Stats ])
                    ]
                , div [ class "col-md-5" ] []
                , button [ class "btn btn-link", onClick BackToBasicInfo ] [ text "Back" ]
                , button [ classList [ ( "btn btn-primary", True ), ( "disabled", not (okToSubmitLeaderForm model) ) ], type_ "submit" ] [ text "Save" ]
                ]

        _ ->
            text ""


len : String -> Int
len string =
    String.length string



-- Need to do better form validation here


okToSubmitLeaderForm : Model -> Bool
okToSubmitLeaderForm model =
    if (len model.currentLeader.email >= 6) && (len model.currentLeader.name >= 2) then
        True

    else
        False


viewLeadersInfo : Dict Int L.Leader -> Html Msg
viewLeadersInfo leaders =
    div []
        (List.map viewLeaderInfo (Dict.values leaders))


viewLeaderInfo : L.Leader -> Html Msg
viewLeaderInfo leader =
    p [ class "card-text", onClick (LoadLeader leader.id) ]
        [ text (L.leaderTypeToString leader.position ++ ": " ++ leader.name ++ " - " ++ leader.email)
        ]


viewInfoBox : Model -> Html Msg
viewInfoBox model =
    case model.progress of
        NotStartedRegistration ->
            text ""

        _ ->
            div [ class "col-md-5 mb-3" ]
                [ div [ class "card", id "basic-info-card" ]
                    [ h5 [ class "card-header" ]
                        [ text "General Info"
                        ]
                    , div [ class "card-body" ]
                        [ h5 [ class "card-title" ]
                            [ text model.name
                            ]
                        , p [ class "card-text" ]
                            [ Loc.viewLocation model.location
                            ]
                        , h5 [ class "card-title" ]
                            [ text "Leaders"
                            ]
                        , viewLeadersInfo model.leaders
                        ]
                    ]
                , viewSubmitRegistration model
                ]



-- Short Description : takes a model, branches on success and shows a message saying thank you


viewSuccessMessage : Model -> Html Msg
viewSuccessMessage model =
    case model.progress of
        FinishedRegistration ->
            div [ class "border rounded p-3 mb-3 ml-3 col-md-6" ]
                [ h5 [ class "card-title" ] [ text "Thank you for registering with Roy Hobbs!" ]
                , div [ class "card-body" ] [ text ("Share this with your teams: " ++ model.sharable_url) ]
                ]

        _ ->
            text ""


viewSubmitRegistration model =
    case model.progress of
        FinishedRegistration ->
            text ""

        _ ->
            case model.readyToSubmit of
                Complete ->
                    button [ class "btn btn-primary btn-lg btn-block", onClick SubmitRegistration ] [ text "Submit Registration" ]

                NotComplete ->
                    text ""


viewFooter model =
    footer [ class "blog-footer" ]
        [ p []
            [ text "Contact Us - "
            , a [ href "mailto:support@royhobbs.com" ] [ text "Email" ]
            , text " - Call : "
            , a [ href "tel:13309233400" ] [ text "1-330-923-3400" ]
            ]
        , p []
            [ text "© 2019 Roy Hobbs Baseball"
            ]
        , p []
            [ text ("userToken available in TeamRegistration: " ++ model.userToken) ]
        ]


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ viewJumbotron model
        , viewProgressBar model
        , div [ class "row justify-content-between" ]
            [ viewSuccessMessage model
            , viewBasicInfoForm model
            , viewLeaderForm model
            , viewInfoBox model
            ]
        , viewFooter model
        ]
