defmodule RsWeb.League.Registration do
  alias __MODULE__

  defstruct state: :initialized, league: :not_set
  # finite state machine for registration steps
  def new(), do: Registration

  def check(%Registration{state: :initialized} = registration, :add_name) do
    {:ok, %Registration{registration | state: :name_set}}
  end

  def check(%Registration{state: :name_set} = registration, {:finished_check, finished_or_not}) do
    case finished_or_not do
      :not_finished -> {:ok, registration}
      :finished -> {:ok, %Registration{registration | state: :registration_finished}}
    end
  end

  def check(_state, _action), do: :error
end
