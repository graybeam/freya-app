defmodule RsWeb.PageControllerTest do
  use RsWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to Registration System!"
  end
end
