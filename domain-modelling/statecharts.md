 # Statecharts for League Registration
 + States
     + Initial State
     + Entering BasicInfo
     + Entering Leadership
     + Submitted

 + Transitions
     + Initial State -> Entering BasicInfo, event: register
     + Entering BasicInfo -> Entering Leadership, event:


+ RegistrationLinkState table
registrationLinkState userState input                  output                nextRegistrationLinkState nextUserState
initial               initial   recivedEmailRequest    sendMagicLink         linkSent                  initial
linkSent              initial   clickedLink            loginOrCreateAndLogin linkClicked               loggedIn
linkClicked           loggedIn  #startRegistration#    teamRegistration      used                      loggedIn              

+ TeamRegistrationState table
