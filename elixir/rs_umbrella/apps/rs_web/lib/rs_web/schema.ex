defmodule RsWeb.Schema do
  use Absinthe.Schema

  import_types RsWeb.Schema.DataTypes

  alias RsWeb.Resolvers

  query do
    @desc "Get a list of leagues"
    field :leagues, list_of(:league) do
      resolve fn _parent, _args, _resolution ->
        {:ok, Rs.Organization.list_leagues()}
      end
    end

    @desc "Get a league from an id"
    field :league, :league do
      arg :id, non_null(:id)
      resolve &Resolvers.League.find_league()/3
    end

    @desc "Get a user of the system"
    field :user, :user do
      arg :id, non_null(:id)
      resolve &Resolvers.Account.find_user/3
    end

    @desc "Get a team"
    field :team, :team do
      arg :id, non_null(:id)
      resolve &Resolvers.Team.find_team()/3
    end

    @desc "Get a link"
    field :hashlink, :hashlink do
      arg :hash, non_null(:id)
      resolve &Resolvers.Organization.find_link()/3
    end

#    @desc "Get a league registration url as a league owner (through user)"
#    field :league_registration, :league_registration do
#      arg :id, non_null(:id)
#    end

  end

  mutation do
    @desc "Create a new league for a given authenticated user"
    field :create_league, type: :league do
      arg :description, non_null(:string)
      arg :name, non_null(:string)
      arg :url, non_null(:string)

      resolve &League.create_league/2
    end

    @desc "Create a rs redirection link for sharing"
    field :create_link, type: :hashlink do
      arg :url, non_null(:string)
      resolve &Resolvers.Organization.create_link/2
    end


  end

end
