defmodule RsWeb.Plug.ConnInterceptor do
  import Plug.Conn, only: [assign: 3]

  def init(opts), do: opts

  def call(conn, _opts) do
    require IEx
    IEx.pry
    conn
  end

end
