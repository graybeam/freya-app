defmodule Rs.Registration.League do
  use GenStateMachine

  #client
  def start_link(), do: GenStateMachine.start_link(__MODULE__, {:not_registering, nil})
  def start(registration), do: GenStateMachine.call(registration, :start)
  def finish(registration), do: GenStateMachine.call(registration, :finish)
  def report(registration), do: GenStateMachine.cast(registration, :report)

  #Server
  def handle_event({:call, from}, :start, :not_registering, data), do: {:next_state, :registering, data, [{:reply, from, :start}]}
  def handle_event(:cast, :report, :not_registering,  _data), do: IO.puts("I am currently not registering") && :keep_state_and_data
  def handle_event(:cast, :report, :registering, _data), do: IO.puts("I am currently registering") && :keep_state_and_data


end