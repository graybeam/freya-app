defmodule Rs.Team.PlayerPool do
  @moduledoc """
  The Organization context.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "player_pools" do
    field :title, :string

    belongs_to :user, Rs.Accounts.User
    has_many :player, Rs.Registration.Player

    timestamps()
  end

  def changeset(player_pool, attrs) do
    player_pool
    |> cast(attrs, [:title])
  end


end