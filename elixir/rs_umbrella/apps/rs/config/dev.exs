# Since configuration is shared in umbrella projects, this file
# should only configure the :rs application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure the mailer
# config :rs, Rs.Mailer,
#   adapter: Bamboo.LocalAdapter


# Configure your database
config :rs, Rs.Repo,
  username: "postgres",
  password: "postgres",
  database: "rs_web_dev",
  hostname: "localhost",
  pool_size: 10
