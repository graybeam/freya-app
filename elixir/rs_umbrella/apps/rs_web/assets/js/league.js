let League = {

    init(socket, element){if(!element){ return }
      let leagueId = element.getAttribute("data-id")
      socket.connect()
      this.onReady(leagueId, socket)
    },
    onReady(leagueId, socket){
      let msgContainer = document.getElementById("msg-container")
      let msgInput = document.getElementById("msg-input")
      let postButton = document.getElementById("msg-submit")
      let lastSeenId = 0
      let leagueChannel = socket.channel("leagues:"+leagueId, () => {
        return {last_seen_id: lastSeenId}
      })

      postButton.addEventListener("click", e => {



          let payload = {body: msgInput.value, at: 13}

          leagueChannel.push("new_annotation", payload)
                    .receive("error", e => console.log(e) )
          msgInput.value = ""
        })

      // msgContainer.addEventListener("click", e => {
      //   e.preventDefault()
      //   let seconds = e.target.getAttribute("data-seek") ||
      //                 e.target.parentNode.getAttribute("data-seek")
      //   if(!seconds){ return }
      //
      //   Player.seekTo(seconds)
      // })

      leagueChannel.on("new_annotation", (resp) => {
        lastSeenId = resp.id
        this.renderAnnotation(msgContainer, resp)
      })

      leagueChannel.join()
        .receive("ok", resp => console.log("joined the league channel", resp) )
        .receive("error", reason => console.log("join failed", reason) )


  },
  formatTime(at){
    let date = new Date(null)
    date.setSeconds(at / 1000)
    return date.toISOString().substr(14, 5)
  },
  esc(str){
   let div = document.createElement("div")
   div.appendChild(document.createTextNode(str))
   return div.innerHTML
 },
 renderAnnotation(msgContainer, {user, body, at}){
    let template = document.createElement("div")
    template.innerHTML = `
    <a href="#" data-seek="${this.esc(at)}">
      <b>${this.esc(user.username)}</b>: ${this.esc(body)}
    </a>
    `
    msgContainer.appendChild(template)
    msgContainer.scrollTop = msgContainer.scrollHeight
  },

  scheduleMessages(msgContainer, annotations){
    clearTimeout(this.scheduleTimer)
    this.schedulerTimer = setTimeout(() => {
      let ctime = Player.getCurrentTime()
      let remaining = this.renderAtTime(annotations, ctime, msgContainer)
      this.scheduleMessages(msgContainer, remaining)
    }, 1000)
  },

  renderAtTime(annotations, seconds, msgContainer){
    return annotations.filter( ann => {
      if(ann.at > seconds){
        return true
      } else {
        this.renderAnnotation(msgContainer, ann)
        return false
      }
    })
  },

}
export default League
