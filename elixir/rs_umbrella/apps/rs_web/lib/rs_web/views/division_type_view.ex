defmodule RsWeb.DivisionTypeView do
  use RsWeb, :view
  alias RsWeb.DivisionTypeView

  def render("index.json", %{division_types: division_types}) do
    %{data: render_many(division_types, DivisionTypeView, "division_type.json")}
  end

  def render("show.json", %{division_type: division_type}) do
    %{data: render_one(division_type, DivisionTypeView, "division_type.json")}
  end

  def render("division_type.json", %{division_type: division_type}) do
    %{id: division_type.id,
      title: division_type.title,
      division: division_type.division}
  end
end
