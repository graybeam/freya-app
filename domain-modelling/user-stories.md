### Bounded context: Team Registration

### UserStory: "Team Manager views other registered teams in league"
      triggered by:
          "Common team list requested" event
      primary input:
          TeamID
      other input:
          LeagueID
      output events:
          "List of common teams viewed"
      side-effects:
          A sortable list of common teams is displayed.

    data TeamID =
       unique team identifier

    data LeagueID =
       unique league identifier


### Bounded context: League Registration

### UserStory: "League Commissioner sees a list of teams registered."
     triggered by:
         "Team list requested" event
     primary input:
         LeagueID
     other input:
         Current Date
     output events:
         "Team list viewed"
     side-effects:
         A sortable list of teams is displayed. A notification is displayed in the commissioner page.

     data LeagueID =
        unique league identifier

### Bounded context: League Registration

### UserStory: "League Commissioner views list of players in league."
     triggered by:
         "Player list requested" event
     primary input:
         LeagueID
     other input:
         Current Date
     output events:
         "Player list viewed"
     side-effects:
         A sortable list of players is displayed, with Team and FirstName / LastName

     data LeagueID =
        unique league identifier


### Bounded context: Team Registration

### UserStory: "Team manager views a list of players registered on the team."
     triggered by:
         "Player list requested" event
     primary input:
         TeamID
     other input:
         Current Date
     output events:
         "Player list viewed"
     side-effects:
         A sortable list of players is displayed. A notification is displayed in the manager page.

   data TeamID =
      unique team identifier
