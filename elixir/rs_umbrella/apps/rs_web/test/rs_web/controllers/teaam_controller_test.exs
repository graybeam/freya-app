defmodule RsWeb.TeaamControllerTest do
  use RsWeb.ConnCase

  alias Rs.Organization

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:teaam) do
    {:ok, teaam} = Organization.create_teaam(@create_attrs)
    teaam
  end

  describe "index" do
    test "lists all teams", %{conn: conn} do
      conn = get(conn, Routes.teaam_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Teams"
    end
  end

  describe "new teaam" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.teaam_path(conn, :new))
      assert html_response(conn, 200) =~ "New Teaam"
    end
  end

  describe "create teaam" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.teaam_path(conn, :create), teaam: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.teaam_path(conn, :show, id)

      conn = get(conn, Routes.teaam_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Teaam"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.teaam_path(conn, :create), teaam: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Teaam"
    end
  end

  describe "edit teaam" do
    setup [:create_teaam]

    test "renders form for editing chosen teaam", %{conn: conn, teaam: teaam} do
      conn = get(conn, Routes.teaam_path(conn, :edit, teaam))
      assert html_response(conn, 200) =~ "Edit Teaam"
    end
  end

  describe "update teaam" do
    setup [:create_teaam]

    test "redirects when data is valid", %{conn: conn, teaam: teaam} do
      conn = put(conn, Routes.teaam_path(conn, :update, teaam), teaam: @update_attrs)
      assert redirected_to(conn) == Routes.teaam_path(conn, :show, teaam)

      conn = get(conn, Routes.teaam_path(conn, :show, teaam))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, teaam: teaam} do
      conn = put(conn, Routes.teaam_path(conn, :update, teaam), teaam: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Teaam"
    end
  end

  describe "delete teaam" do
    setup [:create_teaam]

    test "deletes chosen teaam", %{conn: conn, teaam: teaam} do
      conn = delete(conn, Routes.teaam_path(conn, :delete, teaam))
      assert redirected_to(conn) == Routes.teaam_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.teaam_path(conn, :show, teaam))
      end
    end
  end

  defp create_teaam(_) do
    teaam = fixture(:teaam)
    {:ok, teaam: teaam}
  end
end
