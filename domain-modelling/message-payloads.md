## Message Payloads

    LeagueApiV1 Create League Payload in json

  ```javascript
    {
     "league": {
        "Name": "Captain Marvel League",
        "Status": "URL",
        "Contacts": [
           {
              "FirstName": "Mac Chambers",
              "LastName": "Mac Chambers",
              "EmailAddress": "mchambers@royhobbs.com",
              "MobilePhoneNumber": "3303522174",
              "Title": "Other"
           },
           {
              "FirstName": "Ellen Giffen",
              "LastName": "Ellen Giffen",
              "EmailAddress": "origin@graybeam.tech",
              "MobilePhoneNumber": "3303522174",
              "Title": "Other"
           }
        ],
        "Location": {
           "AddressLine1": "14721 Eagles Lookout Ct.",
           "AddressLine2": "",
           "City": "Ft. Myers",
           "State": "Florida [FL]",
           "PostalCode": "33912",
           "Country": "US"
        },
        "Description": "DESCRIPTION"
     }
    }
    ```
