defimpl Phoenix.Param, for: Rs.Organization.League do
  def to_param(%{slug: slug, id: id}) do
    "#{id}-#{slug}"
  end
end
