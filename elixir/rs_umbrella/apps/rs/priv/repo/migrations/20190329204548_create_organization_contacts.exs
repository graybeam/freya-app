defmodule RsWeb.Repo.Migrations.CreateOrganizationContacts do
  use Ecto.Migration

  def change do
    create table(:contacts) do

      add :name, :string, null: false
      add :email, :string
      add :phone, :string
      add :role, :string, null: false

      add :league_id, references(:leagues, on_delete: :nothing)

      timestamps()
    end

  end
end
