defmodule RsWeb.API.V1.Auth do
    import Plug.Conn
    import Phoenix.Controller

    alias Rs.Accounts
 
    def init(opts), do: opts

    def call(conn, _opts) do
      IO.inspect(conn)
      {"authorization", token} = List.keyfind(conn.req_headers, "authorization",0)

      case Phoenix.Token.verify(conn, "user socket", token) do
        {:ok, user_id} ->
          user = Accounts.get_user(user_id)
          conn
          |> assign(:current_user, user)
          |> assign(:user_token, token)
        {:error, reason} ->
          conn
          |> put_view(RsWeb.API.V1.ErrorView)
          |> render("auth_error.json", reason: reason)
          |> halt()
      end
    end


end
