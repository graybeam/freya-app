defmodule Rs.TestHelpers do
  alias Rs.{
    Accounts,
    Organization
  }

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        name: "Mac user",
        username: username,
        credential: %{
          email: attrs[:email] || "#{username}@royhobbs.com",
          password: attrs[:password] || "supersecret"
        }
      })
      |> Accounts.register_user()

    user
  end

  def league_fixture(%Accounts.User{} = user, attrs \\ %{}) do
    attrs =
      Enum.into(attrs, %{
        title: "A League Title",
        url: "http://example.com",
        description: "a description"
      })

    {:ok, league} = Organization.create_league(user, attrs)

    league
  end
end
