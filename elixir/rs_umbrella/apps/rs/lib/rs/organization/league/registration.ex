defmodule Rs.Organization.League.Registration do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rs.Organization.League
  alias Rs.Organization.Team

  schema "league_registrations" do
    field :name, :string
    field :url, :string

    belongs_to :league, League
    has_many :teams, Team

    timestamps()
  end

  @doc false
  def changeset(league_registration, attrs) do
    league_registration
    |> IO.inspect(label: "in league registration changeset")
    |> cast(attrs, [:name, :url])
    |> cast_assoc(:teams, with: &Team.changeset/2)
  end


end