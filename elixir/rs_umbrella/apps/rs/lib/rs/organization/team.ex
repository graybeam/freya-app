defmodule Rs.Organization.Team do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rs.Organization.Contact

  @primary_key {:id, Rs.Organization.Permalink, autogenerate: true}
  schema "teams" do
    field :name, :string
    field :location, :string
    field :slug, :string

    belongs_to :user, Rs.Accounts.User
    has_many :contacts, Rs.Organization.Contact

    timestamps()
  end

  @doc false
  def changeset(teaam, attrs) do
    teaam
    |> cast(attrs, [:name,:location])
    |> validate_required([:name, :location])
    |> cast_assoc(:contacts, with: &Contact.changeset/2)
    |> slugify_title()
  end

  defp slugify_title(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
  end

end
