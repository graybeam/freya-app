defmodule RsWeb.API.V1.LeagueController do
  use RsWeb, :controller
  require Logger


  alias Rs.Organization

  def index(conn, _params) do
    leagues = Organization.list_leagues()
    render(conn, "index.json", leagues: leagues)
  end

  def show(conn, %{"id" => id}) do
    league = Organization.get_league!(id)
    Logger.debug("League value: #{inspect(league)}")
    render(conn, "show.json", league: league)
  end

  # Copied from the league_controller.. when we create the league, we want to respond through json and not use flash etc
  # create_league takes the current_user and inserts them into Repo.  We need to get the user from session or token in the json payload
  # get it from the token in the json payload
  def create(conn, params,3) do
    Logger.debug("inside bogus create #{inspect(params)}")
    conn
  end
  def create(conn, %{"league" => league_params}) do
    #require IEx when trouble shooting the params
    #IEx.pry()
    current_user = conn.assigns[:current_user]
    case Organization.create_league(current_user, league_params) do
      {:ok, league} ->
        conn
        |> render("ok.json", league: league)
      {:error, %Ecto.Changeset{} = changeset} ->
        Logger.debug(":error in changeset: #{inspect(changeset)}")
        render(conn, "new.json", changeset: changeset)
    end
  end
end
