defmodule RsWeb.Resolvers.League do
  alias Rs.Accounts.User
  alias Rs.Organization.League
  alias Rs.Organization

  @moduledoc """
  mutation CreatePost {
  createLeague(name: "Mac's League of Graphs", description: "We're off to a great start!", url: "http://www.graphql.org") {
    id
  }
  }
  """

  def hello_mac() do

  end

  def find_league(_parent, %{id: id}, _resolution) do
    league = Organization.get_league!(id)
    {:ok, league}
  end

  def list_leagues(%Rs.Accounts.User{} = user, _args, _resolution) do
    {:ok, Organization.list_user_leagues(user)}
  end

  def create_league(_parent, args) do

    #TODO Get user properly added from the authorization token or allow for that
    Rs.Organization.create_league_test(args)
  end


end