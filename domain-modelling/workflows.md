## Bounded context: League Registration

### Workflow: "Register League"
      triggered by:
          "League registration link viewed"
      primary input:
          A league registration form
      other input:
          Past league information
      output events:
          "League is registered", "Completed league registration received by admin"
      side-effects:
          A registration receipt is sent to the league registered email, and then a message is generated in the admin interface

    data LeagueInformation =
        Name
        AND Status
        AND list of ContactInfo
        AND Location
        AND Description

    data Status =
        Active
        OR ActiveMultiYear
        OR Inactive

    data ContactInfo =
        FirstName
        AND LastName
        AND EmailAddress
        AND MobilePhoneNumber
        AND Title

    data Location =
        Address

    data Address =
        AddressLine1
        AND AddressLine2
        AND City
        AND PostalCode
        AND State
        AND Country


## Bounded context: Team Registration

### Workflow: "Register Team"
      triggered by:
          "Team registration link viewed"
      primary input:
          A team registration form
      other input:
          league information
      output events:
          "Team is registered", "Completed team registration received by league commissioner","Team Registration link generated"
      side-effects:
          A registration receipt is sent to the team registered email, and then a message is generated in the league interface

    data TeamInformation =
        TeamName
        AND ContactInfo
        AND Location

    data ContactInfo =
        FirstName
        AND LastName
        AND EmailAddress
        AND MobilePhoneNumber
        AND Title

    data Location =
        Address

    data Address =
        AddressLine1
        AND AddressLine2
        AND City
        AND PostalCode
        AND State
        AND Country


## Bounded context: Player Registration

### Workflow: "Register Player"
      triggered by:
          "Player registration link viewed"
      primary input:
          A player registration form
      other input:
          league information, team information
      output events:
          "Player is registered", "Completed player registration received by team manager"
      side-effects:
          A registration receipt is sent to the player registered email, and then a message is generated in the team interface

    data PlayerInformation =
        ContactInfo
        AND BirthDate
        AND MailingAddress

    data ContactInfo =
        FirstName
        AND LastName
        AND EmailAddress
        AND MobilePhoneNumber
        AND Title

    data MailingAddress =
        Address


## Bounded context: Admin

### Workflow: "Admin views leagues registered"
      triggered by:
          "League list requested" event
      primary input:
          Request league list
      other input:
          DateTime
      output events:
          "League list viewed"
      side-effects:
          A sortable list of leagues is displayed.


## Bounded context: Admin

### Workflow: "Admin views teams in league"
      triggered by:
          "Team list requested for league" event
      primary input:
          Request team list
      other input:
          LeagueID
      output events:
          "Team list for league viewed"
      side-effects:
          A sortable list of teams is displayed.


## Bounded context: Admin

### Workflow: "Admin views players in league"
      triggered by:
          "Player list requested for league" event
      primary input:
          Player list requested
      other input:
          LeagueID
      output events:
          "Player list for league viewed"
      side-effects:
          A sortable list of players is displayed.
