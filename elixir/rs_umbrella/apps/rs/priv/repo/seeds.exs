# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RsWeb.Repo.insert!(%RsWeb.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Rs.Organization

for category <- ~w(Individual Member-League Other) do
  Organization.create_category(category)
end
