defmodule Rs.League.DivisionType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "division_types" do
    field :division, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(division_type, attrs) do
    division_type
    |> cast(attrs, [:title, :division])
    |> validate_required([:title, :division])
  end
end
