module Officer exposing (Officer, OfficerType(..), encode, initialOfficer, officerTypeToString)

import Json.Encode as E


type OfficerType
    = Secretary
    | President
    | Commissioner
    | Other


type alias Officer =
    { id : Int
    , name : String
    , email : String
    , phone : String
    , position : OfficerType
    }


initialOfficer : Officer
initialOfficer =
    { id = 0
    , name = ""
    , email = ""
    , phone = ""
    , position = Commissioner
    }


encode : Officer -> E.Value
encode officer =
    E.object
        [ ( "name", E.string officer.name )
        , ( "email", E.string officer.email )
        , ( "phone", E.string officer.phone )
        , ( "role", E.string (officerTypeToString officer.position) ) -- TODO match up positions with the API
        ]


officerTypeToString : OfficerType -> String
officerTypeToString officerType =
    case officerType of
        Secretary ->
            "Secretary"

        President ->
            "President"

        Commissioner ->
            "Commissioner"

        Other ->
            "Other"
