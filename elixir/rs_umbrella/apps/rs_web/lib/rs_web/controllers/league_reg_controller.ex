defmodule RsWeb.LeagueRegController do
  require Logger
  use RsWeb, :controller
  alias Rs.Organization
  alias Rs.Organization.League


  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.current_user]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def show(conn, %{"messenger" => messenger} = params) do
    render(conn, "show.html", messenger: messenger)
  end

  def league_magic_link(conn, %{"id" => id}) do
    current_user = Rs.Accounts.get_user(1)
    league = Rs.Organization.get_league!(id)
    render(conn, "league_reg.html", league: league)
  end

  def register(conn, %{"id" => id}) do
    league = Rs.Organization.get_league!(id)
    render(conn, "register.html", league: league)
  end

  def new(conn, _params, current_user) do
    Logger.info("inside of the league reg controller  ")
    changeset = Organization.change_league(current_user, %League{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, params) do
    Logger.info("Creating an object from JSON: #{inspect(params)}")
    render(conn, "show.json", params)
  end
end
