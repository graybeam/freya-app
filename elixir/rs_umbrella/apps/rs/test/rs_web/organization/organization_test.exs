defmodule RsWeb.OrganizationTest do
  use RsWeb.DataCase

  alias RsWeb.Organization
  alias RsWeb.Organization.Category

  describe "categories" do
    test "list_alphabetical_categories/0" do
      for name <- ~w(Other Member Non-Member), do: Organization.create_category(name)

      alpha_names =
        for %Category{name: name} <- Organization.list_alphabetical_categories() do
          name
        end

      assert alpha_names == ~w(Member Non-Member Other)
    end
  end

  describe "leagues" do
    alias RsWeb.Organization.League

    @valid_attrs %{description: "desc", title: "title", url: "http://local"}
    @invalid_attrs %{description: nil, title: nil, url: nil}

    test "list_leagues/0 returns all leagues" do
      owner = user_fixture()
      %League{id: id1} = league_fixture(owner)
      assert [%League{id: ^id1}] = Organization.list_leagues()
      %League{id: id2} = league_fixture(owner)
      assert [%League{id: ^id1}, %League{id: ^id2}] = Organization.list_leagues()
    end

    test "get_league!/1 returns the league with given id" do
      owner = user_fixture()
      %League{id: id} = league_fixture(owner)
      assert %League{id: ^id} = Organization.get_league!(id)
    end

    test "create_league/2 with valid data creates a league" do
      owner = user_fixture()
      assert {:ok, %League{} = league} = Organization.create_league(owner, @valid_attrs)
      assert league.description == "desc"
      assert league.title == "title"
      assert league.url == "http://local"
    end

    test "create_league/2 with invalid data returns error changeset" do
      owner = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.create_league(owner, @invalid_attrs)
    end

    test "update_league/2 with valid data updates the league" do
      owner = user_fixture()
      league = league_fixture(owner)
      assert {:ok, league} = Organization.update_league(league, %{title: "updated title"})
      assert %League{} = league
      assert league.title == "updated title"
    end

    test "update_league/2 with invalid data returns error changeset" do
      owner = user_fixture()
      %League{id: id} = league = league_fixture(owner)
      assert {:error, %Ecto.Changeset{}} = Organization.update_league(league, @invalid_attrs)
      assert %League{id: ^id} = Organization.get_league!(id)
    end

    test "delete_league/1 deletes the league" do
      owner = user_fixture()
      league = league_fixture(owner)
      assert {:ok, %League{}} = Organization.delete_league(league)
      assert Organization.list_leagues() == []
    end

    test "change_league/2 returns a league changeset" do
      owner = user_fixture()
      league = league_fixture(owner)
      assert %Ecto.Changeset{} = Organization.change_league(owner, league)
    end
  end
end
