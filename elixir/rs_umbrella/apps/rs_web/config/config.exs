# Since configuration is shared in umbrella projects, this file
# should only configure the :rs_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :rs_web,
  ecto_repos: [Rs.Repo],
  generators: [context_app: :rs]

# Configures the endpoint
config :rs_web, RsWeb.Mailer,
    adapter: Bamboo.SendGridAdapter,
    api_key: "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"

config :rs_web, RsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "N126SyJ3+ECAia9fqDTTRb8YgimqJDDGZPuk8YJ23BC+ocjVTP4UcOE8+5jdhCKh",
  render_errors: [view: RsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RsWeb.PubSub, adapter: Phoenix.PubSub.PG2]

config :phoenix, :format_encoders,
    json: Jason

config :bamboo, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
