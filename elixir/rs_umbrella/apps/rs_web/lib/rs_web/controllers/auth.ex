defmodule RsWeb.Auth do
  import Plug.Conn
  import Phoenix.Controller

  require Logger
  alias RsWeb.Router.Helpers, as: Routes
  alias Rs.Accounts

  def init(opts), do: opts

  def login(conn, user) do
    conn
    |> put_current_user(user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)

    cond do
      user = conn.assigns[:current_user] ->
        put_current_user(conn, user)

      user = user_id && Accounts.get_user(user_id) ->
        put_current_user(conn, user)

      true ->
        assign(conn, :current_user, nil)
    end
  end

  def login_by_email_and_pass(conn, email, given_pass) do
    Logger.info("user from the system: #{inspect(given_pass)}")

    case Accounts.authenticate_by_email_and_pass(email, given_pass) do
      {:ok, user} -> {:ok, login(conn, user)}
      {:error, :unauthorized} -> {:error, :unauthorized, conn}
      {:error, :not_found} -> {:error, :not_found, conn}
    end
  end

  def authenticate_user(conn, _opts) do
    Logger.info("In authenticate_user(conn, opts)")
    Logger.debug("#{inspect(conn)}")

    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must login to access that. Thank you :-) ")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    end
  end

  def authenticate_user_from_token(conn,_opts) do
    {"authorization", token} = List.keyfind(conn.req_headers, "authorization",0)

    case Phoenix.Token.verify(conn, "user_socket", token) do
      {:ok, user_id} ->
        user = Accounts.get_user(user_id)
        conn
        |> assign(:current_user,user)
        |> assign(:user_token, token) # just matching the put_current_user
      {:error, _reason} -> 
        conn
        |> halt()
    end
  end

  defp put_current_user(conn, user) do
    token = Phoenix.Token.sign(conn, "user socket", user.id)

    conn
    |> assign(:current_user, user)
    |> assign(:user_token, token)
  end
end
