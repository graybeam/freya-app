## Events
- Register league link sent
- Register league link received
- Register league form completed
- League is registered
- Completed league registration is received by admin
- Register team link sent
- Register team link received
- Team is registered
- Completed team registration is received by league commissioner
- Register player link sent
- Register player link received
- Player is registered
- Completed Player registration received by team manager
- List of common teams viewed
- Common team list requested
- Team list requested
- Team list viewed
- Player list requested
- Player list viewed
- League list requested (admin)
- League list viewed
- Team list requested for league
- Player list for league viewed

## Commands
- Send league registration link
- View league registration link
- Complete league registration form
- Register league
- View completed league registration
- Send register team link
- View team registration link
- View completed team registration
- Register team
- Receive completed team registration
- Send register player link
- Register player
- View completed player registration
- Receive completed player registration
- Show a list of common teams
- Request common team list
- Request team list (Commissioner)
- Show a list of players (Manager)
- Request a list of players (Manager)
- Request league list (admin)
- Request team list for league (admin)
- Request player list (admin)
