defmodule RsWeb.LeagueControllerTest do
  use RsWeb.ConnCase

  alias RsWeb.Organization

  @create_attrs %{url: "http://youtu.be", title: "vid", description: "a vid"}
  @invalid_attrs %{title: "invalid"}

  defp league_count, do: Enum.count(Organization.list_leagues())

  describe "with a logged-in user" do
    setup %{conn: conn, login_as: username} do
      user = user_fixture(username: username)
      conn = assign(conn, :current_user, user)

      {:ok, conn: conn, user: user}
    end

    @tag login_as: "max"
    test "lists all user's leagues on index", %{conn: conn, user: user} do
      user_league = league_fixture(user, title: "funny cats")
      other_league = league_fixture(user_fixture(username: "other"), title: "another league")

      conn = get(conn, Routes.league_path(conn, :index))
      assert html_response(conn, 200) =~ ~r/Listing Leagues/
      assert String.contains?(conn.resp_body, user_league.title)
      refute String.contains?(conn.resp_body, other_league.title)
    end

    @tag login_as: "max"
    test "creates user league and redirects", %{conn: conn, user: user} do
      create_conn = post conn, Routes.league_path(conn, :create), league: @create_attrs

      assert %{id: id} = redirected_params(create_conn)
      assert redirected_to(create_conn) == Routes.league_path(create_conn, :show, id)

      conn = get(conn, Routes.league_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show League"

      assert Organization.get_league!(id).user_id == user.id
    end

    @tag login_as: "max"
    test "does not create league and renders errors when invalid", %{conn: conn} do
      count_before = league_count()
      conn = post conn, Routes.league_path(conn, :create), league: @invalid_attrs
      assert html_response(conn, 200) =~ "check the errors"
      assert league_count() == count_before
    end
  end

  test "authorizes actions against access by other users", %{conn: conn} do
    owner = user_fixture(username: "owner")
    league = league_fixture(owner, @create_attrs)
    non_owner = user_fixture(username: "sneaky")
    conn = assign(conn, :current_user, non_owner)

    assert_error_sent :not_found, fn ->
      get(conn, Routes.league_path(conn, :show, league))
    end

    assert_error_sent :not_found, fn ->
      get(conn, Routes.league_path(conn, :edit, league))
    end

    assert_error_sent :not_found, fn ->
      put(conn, Routes.league_path(conn, :update, league, league: @create_attrs))
    end

    assert_error_sent :not_found, fn ->
      delete(conn, Routes.league_path(conn, :delete, league))
    end
  end

  test "requires user authentication on all actions", %{conn: conn} do
    Enum.each(
      [
        get(conn, Routes.league_path(conn, :new)),
        get(conn, Routes.league_path(conn, :index)),
        get(conn, Routes.league_path(conn, :show, "123")),
        get(conn, Routes.league_path(conn, :edit, "123")),
        put(conn, Routes.league_path(conn, :update, "123", %{})),
        post(conn, Routes.league_path(conn, :create, %{})),
        delete(conn, Routes.league_path(conn, :delete, "123"))
      ],
      fn conn ->
        assert html_response(conn, 302)
        assert conn.halted
      end
    )
  end
end
