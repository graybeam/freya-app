defmodule RsWeb.LeagueController do
  use RsWeb, :controller
  require Logger

  alias Rs.Organization
  alias Rs.Organization.League
  alias Rs.{Mailer, Email}

  plug :load_categories when action in [:new, :create, :edit, :update]

  def index(conn, _params, current_user) do
    leagues = Organization.list_user_leagues(current_user)
    render(conn, "index.html", leagues: leagues)
  end

  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.current_user]
    apply(__MODULE__, action_name(conn), args)
  end

  def new(conn, _params, current_user) do
    changeset = Organization.change_league(current_user, %League{})
    Logger.info("changeset in new league #{inspect changeset} ")
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"league" => league_params}, current_user) do
    Logger.info('league_params coming in...')
    Logger.debug('league #{inspect(league_params)}')
    case Organization.create_league(current_user, league_params) do
      {:ok, league} ->
        conn
        |> put_flash(:info, "League created successfully.")
        |> redirect(to: Routes.league_path(conn, :show, league))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, current_user) do
    league = Organization.get_user_league!(current_user, id)
    render(conn, "show.html", league: league)
  end

  def edit(conn, %{"id" => id}, current_user) do
    league = Organization.get_user_league!(current_user, id)
    changeset = Organization.change_league(current_user, league)
    render(conn, "edit.html", league: league, changeset: changeset)
  end

  def update(conn, %{"id" => id, "league" => league_params}, current_user) do
    league = Organization.get_user_league!(current_user, id)

    case Organization.update_league(league, league_params) do
      {:ok, league} ->
        conn
        |> put_flash(:info, "League updated successfully.")
        |> redirect(to: Routes.league_path(conn, :show, league))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", league: league, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    league = Organization.get_user_league!(current_user, id)
    {:ok, _league} = Organization.delete_league(league)

    conn
    |> put_flash(:info, "League deleted successfully.")
    |> redirect(to: Routes.league_path(conn, :index))
  end

  defp load_categories(conn, _) do
    assign(conn, :categories, Organization.list_alphabetical_categories())
  end
end
