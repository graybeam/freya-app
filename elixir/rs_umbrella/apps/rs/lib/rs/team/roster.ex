defmodule Rs.Team.Roster do
  use Ecto.Schema
  import Ecto.Changeset

  schema "rosters" do
    field :title, :string
    field :context, :string

    belongs_to :team, Rs.Team
    has_many :registration, Rs.Organization.League.Registration

    timestamps()
  end

  def changeset(roster, attrs) do
    roster
    |> cast(attrs, [:title, :context])
  end

end