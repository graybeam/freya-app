defmodule RsWeb.Link.HashId do
  @behaviour Ecto.Type
  @hash_id_length 8

  # Ecto Specific Callbacks
  @spec cast(any) :: Map.t()
  def cast(value), do: hash_id_format(value)

  @spec dump(any) :: Map.t
  def dump(value), do: hash_id_format(value)

  @spec load(any) :: Map.t()
  def load(value), do: hash_id_format(value)

  @spec autogenerate() :: String.t()
  def autogenerate, do: generate()

  def type, do: :string


  @spec hash_id_format(any) :: Map.t
  def hash_id_format(value) do
    case validate_hash_id(value) do
      true -> {:ok, value}
      _ -> {:error, "'#{value}' is not a string"}
    end
  end

  @doc "Validate the given value as a string"
  def validate_hash_id(string) when is_binary(string), do: true
  def validate_hash_id(other), do: false

  @spec generate() :: String.t()
  def generate() do
    @hash_id_length
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64()
    |> binary_part(0, @hash_id_length) # picks from 0 to @hash_id_length of the pipe_string
  end
end