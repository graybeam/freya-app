defmodule RsWeb.Resolvers.Account do
  def find_user(_parent, %{id: id}, _resolution) do
    user = Rs.Accounts.get_user(id)
    {:ok, user}
  end
end