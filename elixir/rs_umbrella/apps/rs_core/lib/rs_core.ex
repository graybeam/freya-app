defmodule RSCore do
  @moduledoc """
  Documentation for RSCore.
  """

  @doc """
  Hello world.

  ## Examples

      iex> RSCore.hello()
      :world

  """
  def hello do
    :world
  end
end
