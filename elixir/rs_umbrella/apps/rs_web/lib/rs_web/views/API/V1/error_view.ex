defmodule RsWeb.API.V1.ErrorView do
    use RsWeb, :view
    require Logger

    def render("auth_error.json", %{reason: reason}) do
      %{status: "error", message: "Not Authorized #{reason}", id: "1313131313", request_id: "1313131313"}
    end
end
