defmodule Rs.Repo.Migrations.UpdateLeague do
  use Ecto.Migration

  def change do
    alter table(:leagues) do
      add :location, :string, size: 48
      add :status, :string, default: "Status"
    end

    rename table(:leagues), :title, to: :name


  end
end
