defmodule Rs.League.Season do
  use Ecto.Schema
  import Ecto.Changeset

  schema "divisions" do
    field :title, :string
    field :start_date, :date
    field :end_date, :date

    belongs_to :league, Rs.Organization.League
    has_many :divisions, Rs.League.Division
    timestamps()
  end

  @doc false
  def changeset(season, attrs) do
    season
    |> cast(attrs, [:title, :start_date, :end_date])
    |> validate_required([:title])
  end
end