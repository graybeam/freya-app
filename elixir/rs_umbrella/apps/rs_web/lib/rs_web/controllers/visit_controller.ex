defmodule RsWeb.VisitController do
  use RsWeb, :controller
  alias Rs.Organization

  def show(conn, %{"id" => id}) do
    league = Organization.get_league!(id)
    render(conn, "show.html", league: league)
  end
end
