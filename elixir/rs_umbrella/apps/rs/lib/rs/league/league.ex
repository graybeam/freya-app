defmodule RsWeb.League do
  @moduledoc """
  The Leagues context.
  """

  use GenServer

  alias RsWeb.League.{Contact, Registration}

  @enforce_keys [:name]
  @derive {Jason.Encoder, only: [:name, :description]}
  defstruct name: nil, status: :unregistered, contact: nil, location: nil, description: nil

  # create struct:#> %League{name: "NEORH", status: :started_registration}
  def start_link(name) when is_binary(name) do
    GenServer.start_link(__MODULE__, name, [])
  end

  def lookup(server, league) do
    GenServer.call(server, {:lookup, league})
  end

  # Change these function names to match the workflows
  def create(server, league) do
    GenServer.cast(server, {:create, league})
  end

  def demo_call(league) do
    GenServer.call(league, :demo_call)
  end

  def demo_cast(pid, new_value) do
    GenServer.cast(pid, {:demo_cast, new_value})
  end

  ## Server Callbacks

  def init(name) do
    {:ok, %RsWeb.League{name: name, contact: Contact.new(), description: "Poooooop"}}
  end

  def handle_call(:demo_call, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:lookup, league}, _from, leagues) do
    {:reply, Map.fetch(leagues, league), leagues}
  end

  def handle_cast({:create, league}, leagues) do
    if Map.has_key?(leagues, league) do
      {:noreply, leagues}
    end
  end

  def handle_cast({:demo_cast, new_value}, state) do
    {:noreply, Map.put(state, :test, new_value)}
  end

  def handle_info(:first, state) do
    IO.puts("This message has been handled by handle_info/2, matching on :first.")
    {:noreply, state}
  end
end
