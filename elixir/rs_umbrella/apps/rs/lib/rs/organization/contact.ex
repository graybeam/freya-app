defmodule Rs.Organization.Contact do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  schema "contacts" do
    field :name, :string
    field :email, :string
    field :phone, :string
    field :role, :string

    belongs_to :league, Rs.Organization.League

    timestamps()
  end

  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [:name, :email, :phone, :role])
    |> validate_required([:name, :role])
  end

  def alphabetical(query) do
    from c in query, order_by: c.name
  end
end
