defmodule InfoSys.Weather do
  import SweetXml
  alias InfoSys.Result

  require Logger

  @behaviour InfoSys.Backend

  @base "https://w1.weather.gov/xml/current_obs/"

  #file name looks like this: KDTO.xml

  @impl true
  def name, do: "weather"

  @impl true
  def compute(query_str, _opts) do
    query_str
    |> fetch_xml()
    |> xpath(~x"//temp_f/text()")
    |> build_results()
  end

  defp build_results(nil), do: []
  defp build_results(answer) do
    [%Result{backend: __MODULE__, score: 95, text: to_string(answer)}]
  end

  defp fetch_xml(query) do
    {:ok, {_, _, body}} = :httpc.request(:get, {String.to_charlist(url(query)),[{'User-Agent','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}]},[],[])
    body
  end

  defp url(input) do
    #"#{@base}\/"<>URI.encode_query(appid: id(), input: input, format: "plaintext")
    "#{@base}/"<>input<>".xml"
  end

  defp id(), do: Application.get_env(:info_sys, :weather)[:app_id]
end