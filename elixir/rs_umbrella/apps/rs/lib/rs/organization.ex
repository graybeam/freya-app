defmodule Rs.Organization do
  @moduledoc """
  The Organization context.
  """

  import Ecto.Query, warn: false
  alias Rs.Repo

  alias Rs.Organization.Category
  alias Rs.Organization.League
  alias Rs.Accounts
  alias Rs.Organization.Annotation
  alias Rs.Organization.Contact

  alias RsWeb.Router.Helpers, as: Routes
  alias RsWeb.Endpoint

  @doc """
  Returns the list of leagues.

  ## Examples

      iex> list_leagues()
      [%League{}, ...]

  """

  def create_category(name) do
    Repo.get_by(Category, name: name) || Repo.insert!(%Category{name: name})
  end

  def list_leagues do
    League
    |> IO.inspect(label: "Is this the league struct??")
    |> Repo.all()
    |> preload_user()
    |> preload_contacts()
  end

  def list_user_leagues(%Accounts.User{} = user) do
    League
    |> user_leagues_query(user)
    |> Repo.all()
    |> preload_user()
  end

  def list_alphabetical_categories do
    Category
    |> Category.alphabetical()
    |> Repo.all()
  end

  def list_league_contacts(%League{} = league) do
    Contact
    |> league_contacts_query(league)
    |> Repo.all()
    |> preload_contacts()
  end

  def list_league_teams(%League{} = league, %Rs.Organization.League.Registration{} = league_registration) do
    Rs.Organization.Team
    |> league_teams_query(league, league_registration)
    |> preload_teams()
  end

  def get_user_league!(%Accounts.User{} = user, id) do
    from(l in League, where: l.id == ^id)
    |> user_leagues_query(user)
    |> Repo.one!()
    |> preload_user()
  end

  @doc """
  Gets a single league.

  Raises `Ecto.NoResultsError` if the League does not exist.

  ## Examples

      iex> get_league!(123)
      %League{}

      iex> get_league!(456)
      ** (Ecto.NoResultsError)

  """
  def get_league!(id) do
    Repo.get!(League, id)
    |> preload_user()
    |> preload_contacts()
  end


  def get_league_registration!(id) do
    Repo.get!(Rs.Organization.League.Registration, id)
    |> preload_teams()
  end
  @doc """
  Creates a league.

  ## Examples

      iex> create_league(%{field: value})
      {:ok, %League{}}

      iex> create_league(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_league(%Accounts.User{} = user, attrs \\ %{}) do
    %League{}
    |> League.changeset(attrs)
    |> create_league_registration()
    |> put_user(user)
    |> Repo.insert()
  end

  def create_league_registration(%League{} = league, attrs \\ %{}) do
    %League.Registration{}
    |> IO.inspect(label: "arguments being passed to Registration changeset")
    |> League.Registration.changeset(%{"name" => league.name,"url" => create_url(league.id) })
    |> Repo.insert()
  end

  @doc """
  creates a hash_id url using create_link for distributing to users that want to register a team
  with the given league
  """
  def create_url(league_id) do
    {:ok, %{hash: hash_id }} = create_link(%{"url" => Routes.league_reg_url(Endpoint, :register, league_id)})
    Routes.link_url(Endpoint, :get_and_redirect, hash_id)
  end


  def create_league_test( attrs \\ %{}) do
    create_league(Accounts.get_user(8), attrs.arguments)
  end

  @doc """
  Updates a league.

  ## Examples

      iex> update_league(league, %{field: new_value})
      {:ok, %League{}}

      iex> update_league(league, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_league(%League{} = league, attrs) do
    league
    |> League.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a League.

  ## Examples

      iex> delete_league(league)
      {:ok, %League{}}

      iex> delete_league(league)
      {:error, %Ecto.Changeset{}}

  """
  def delete_league(%League{} = league) do
    Repo.delete(league)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking league changes.

  ## Examples

      iex> change_league(league)
      %Ecto.Changeset{source: %League{}}

  """
  def change_league(%Accounts.User{} = user, %League{} = league) do
    league
    |> League.changeset(%{})
    |> put_user(user)
  end

  defp preload_contacts(league_or_leagues) do
    Repo.preload(league_or_leagues, :contacts)
  end

  defp preload_teams(league_registration_or_registrations) do
    Repo.preload(league_registration_or_registrations, :teams)
  end

  defp put_user(changeset, user) do
    Ecto.Changeset.put_assoc(changeset, :user, user)
  end

  defp user_leagues_query(query, %Accounts.User{id: user_id}) do
    from(l in query, where: l.user_id == ^user_id)
  end


  defp league_contacts_query(query, %League{id: league_id}) do
    from(c in query, where: c.league_id == ^league_id)
  end

  defp league_teams_query(query, %League{id: league_id}, %Rs.Organization.League.Registration{id: league_registration_id}) do
    from(t in query, where: t.league_id == ^league_id)
  end

  defp preload_user(league_or_leagues) do
    Repo.preload(league_or_leagues, :user)
  end

  def annotate_league(%Accounts.User{} = user, league_id, attrs) do
    %Annotation{league_id: league_id}
    |> Annotation.changeset(attrs)
    |> put_user(user)
    |> Repo.insert()
  end

  # def add_contacts_to_league(%League{} = league, attrs) do
  #   Map.
  # end

  def add_contact_to_league(league_id, attrs) do
    %Contact{league_id: league_id}
    |> Contact.changeset(attrs)
    |> Repo.insert()
  end

  def list_annotations(%League{} = league, since_id \\ 0) do
    Repo.all(
      from a in Ecto.assoc(league, :annotations),
        where: a.id > ^since_id,
        order_by: [asc: a.at, asc: a.id],
        limit: 500,
        preload: [:user]
    )
  end

  def list_contacts(%League{} = league) do
    Repo.all(from(c in Ecto.assoc(league, :contacts)))
  end

  alias Rs.Organization.Team

  @doc """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Team{}, ...]

  """
  def list_teams do
    Repo.all(Team)
  end


  def list_user_teams(%Accounts.User{} = user) do
    Team
    |> user_teams_query(user)
    |> Repo.all()
    |> preload_user()
  end
  @doc """
  Gets a single team.

  Raises `Ecto.NoResultsError` if the Team does not exist.

  ## Examples

      iex> get_team!(123)
      %Team{}

      iex> get_team!(456)
      ** (Ecto.NoResultsError)

  """
  def get_team!(id), do: Repo.get!(Team, id)

  @doc """
  Creates a team.

  ## Examples

      iex> create_team(%{field: value})
      {:ok, %Team{}}

      iex> create_team(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_team(%Accounts.User{} = user,attrs \\ %{}) do
    %Team{}
    |> Team.changeset(attrs)
    |> put_user(user)
    |> Repo.insert()
  end

  @doc """
  Updates a team.

  ## Examples

      iex> update_team(team, %{field: new_value})
      {:ok, %Team{}}

      iex> update_team(team, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_team(%Team{} = team, attrs) do
    team
    |> Team.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Team.

  ## Examples

      iex> delete_team(team)
      {:ok, %Team{}}

      iex> delete_team(team)
      {:error, %Ecto.Changeset{}}

  """
  def delete_team(%Team{} = team) do
    Repo.delete(team)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking team changes.

  ## Examples

      iex> change_team(team)
      %Ecto.Changeset{source: %Team{}}

  """
  def change_team(%Team{} = team) do
    Team.changeset(team, %{})
  end

  def get_user_team!(%Accounts.User{} = user, id) do
    from(t in Team, where: t.id == ^id)
    |> user_teams_query(user)
    |> Repo.one!()
    |> preload_user()
  end

  defp user_teams_query(query, %Accounts.User{id: user_id}) do
    from(t in query, where: t.user_id == ^user_id)
  end



  alias RsWeb.Organization.Link

  @doc """
  Returns the list of links.

  ## Examples

      iex> list_links()
      [%Link{}, ...]

  """
  def list_links do
    Repo.all(Link)
  end

  @doc """
  Gets a single link.

  Raises `Ecto.NoResultsError` if the Link does not exist.

  ## Examples

      iex> get_link!(123)
      %Link{}

      iex> get_link!(456)
      ** (Ecto.NoResultsError)

  """
  def get_link!(id), do: Repo.get!(Link, id)

  @doc """
  Creates a link.

  ## Examples

      iex> create_link(%{field: value})
      {:ok, %Link{}}

      iex> create_link(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_link(attrs \\ %{}) do
    %Link{}
    |> Link.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a link.

  ## Examples

      iex> update_link(link, %{field: new_value})
      {:ok, %Link{}}

      iex> update_link(link, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_link(%Link{} = link, attrs) do
    link
    |> Link.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Link.

  ## Examples

      iex> delete_link(link)
      {:ok, %Link{}}

      iex> delete_link(link)
      {:error, %Ecto.Changeset{}}

  """
  def delete_link(%Link{} = link) do
    Repo.delete(link)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking link changes.

  ## Examples

      iex> change_link(link)
      %Ecto.Changeset{source: %Link{}}

  """
  def change_link(%Link{} = link) do
    Link.changeset(link, %{})
  end
end
