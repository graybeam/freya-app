defmodule Rs.Team do
  @moduledoc """
  The League context.
  """

  import Ecto.Query, warn: false
  alias Rs.Repo

  alias Rs.Team.Roster
  alias Rs.Organization.Team

  @doc """
  Returns the list of rosters.

  ## Examples

      iex> list_rosters()
      [%Roster{}, ...]

  """
  def list_rosters do
    Repo.all(Roster)
  end

  @doc """
  Gets a single roster.

  Raises `Ecto.NoResultsError` if the Division type does not exist.

  ## Examples

      iex> get_roster!(123)
      %Roster{}

      iex> get_roster!(456)
      ** (Ecto.NoResultsError)

  """
  def get_roster!(id), do: Repo.get!(Roster, id)

  @doc """
  Creates a roster.

  ## Examples

      iex> create_roster(%{field: value})
      {:ok, %Roster{}}

      iex> create_roster(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_roster(%Team{} = team, attrs \\ %{}) do
    %Roster{}
    |> Roster.changeset(team.id, attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a roster.

  ## Examples

      iex> update_roster(roster, %{field: new_value})
      {:ok, %Roster{}}

      iex> update_roster(roster, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_roster(%Roster{} = roster, attrs) do
    roster
    |> Roster.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Roster.

  ## Examples

      iex> delete_roster(roster)
      {:ok, %Roster{}}

      iex> delete_roster(roster)
      {:error, %Ecto.Changeset{}}

  """
  def delete_roster(%Roster{} = roster) do
    Repo.delete(roster)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking roster changes.

  ## Examples

      iex> change_roster(roster)
      %Ecto.Changeset{source: %Roster{}}

  """
  def change_roster(%Roster{} = roster) do
    Roster.changeset(roster, %{})
  end
end
