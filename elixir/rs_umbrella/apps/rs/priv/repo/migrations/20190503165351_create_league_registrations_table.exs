defmodule Rs.Repo.Migrations.CreateLeagueRegistrationsTable do
  use Ecto.Migration

  def change do
    create table(:league_registrations) do
      add :name, :string, null: false
      add :url, :string, null: false

      add :league_id, references(:leagues, on_delete: :nothing)

      timestamps()
    end
  end
end
