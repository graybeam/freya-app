port module LeagueRegistration exposing (League, Model, Msg(..), Progress(..), encodeLeague, encodeOfficersDict, initialLocation, initialModel, initialOfficersDict, main, statusBar, update, updateCurrentOfficerEmail, updateCurrentOfficerName, updateCurrentOfficerPhone, updateCurrentOfficerType, updateLocation, updateLocationCity, updateLocationLine1, updateLocationLine2, updateLocationPostalCode, updateLocationState, updateOfficerDict, view, viewAddressForm, viewBasicInfoForm, viewFooter, viewHeader, viewInfoBox, viewJumbotron, viewOfficerForm, viewOfficerInfo, viewOfficerTypeChooser, viewOfficersInfo, viewProgressBar)

import Browser
import Dict exposing (Dict)
import Html exposing (Attribute, Html, a, button, div, footer, form, h1, h5, header, img, input, label, li, nav, option, p, pre, select, span, text, ul)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Decode exposing (Decoder, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Enc
import Location as L
import Officer as O
import String


maxOfficers =
    3


zapierUrl =
    "https://hooks.zapier.com/hooks/catch/1507215/ino9qd/"


phoenixBaseUrl =
    "http://localhost:4001/"


phoenixApiUrl =
    "http://localhost:4000/api/v1/leagues"


postmanMockApiUrl =
    "https://17b6920b-8645-49c5-b8db-def5d783296a.mock.pstmn.io/api/v1/leagues"


type alias FeedbackMsg =
    { name : String, description : String }


type alias League =
    { name : String
    , officers : List O.Officer
    , location : L.Location
    }


type Progress
    = EnteringBasicInfo
    | EnteringOfficerInfo
    | FinishedRegistration
    | NotStartedRegistration


type ReadyToSubmit
    = Complete
    | NotComplete



--type OfficerDict
--    = Dict Int Officer


type alias Model =
    { name : String
    , location : L.Location
    , officers : Dict Int O.Officer
    , progress : Progress
    , readyToSubmit : ReadyToSubmit
    , currentOfficer : O.Officer
    , currentLocation : L.Location
    , sharable_url : String
    , userToken : String
    }


type alias Submission =
    { status : String
    , message : String
    , id : String
    , request_id : String
    , sharable_url : String
    }


initialLocation : L.Location
initialLocation =
    L.initialLocation


initialOfficersDict : Dict Int O.Officer
initialOfficersDict =
    Dict.fromList [ ( 0, O.initialOfficer ) ]


initialModel : String -> Model
initialModel userToken =
    { officers = Dict.empty --Dict.fromList [ ( 0, { id = 0, name = "", email = "", phone = "", position = Commissioner } ) ]
    , location = L.initialLocation
    , name = "League Name"
    , progress = EnteringBasicInfo
    , readyToSubmit = NotComplete
    , currentOfficer = O.initialOfficer
    , currentLocation = initialLocation
    , userToken = userToken
    , sharable_url = ""
    }


type Msg
    = ClickedOfficerType O.OfficerType
    | SaveBasicInfo
    | SaveCurrentOfficer Int
    | UpdateLeagueName String
    | UpdateLocationLine1 String
    | UpdateLocationLine2 String
    | UpdateLocationCity String
    | UpdateLocationState String
    | UpdateLocationPostalCode String
    | UpdateCurrentOfficerName String
    | UpdateCurrentOfficerEmail String
    | UpdateCurrentOfficerPhone String
    | StartRegistration
    | LoadOfficer Int
    | BackToBasicInfo
    | ReceivedInfo (Result Http.Error Submission)
    | SubmitRegistration


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SaveBasicInfo ->
            ( { model | progress = EnteringOfficerInfo }, Cmd.none )

        ClickedOfficerType officerType ->
            ( updateCurrentOfficerType officerType model, Cmd.none )

        UpdateLeagueName name ->
            ( { model | name = name }, Cmd.none )

        UpdateLocationLine1 line1 ->
            ( updateLocationLine1 line1 model, Cmd.none )

        UpdateLocationLine2 line2 ->
            ( updateLocationLine2 line2 model, Cmd.none )

        UpdateLocationCity city ->
            ( updateLocationCity city model, Cmd.none )

        UpdateLocationState state ->
            ( updateLocationState state model, Cmd.none )

        UpdateLocationPostalCode postal_code ->
            ( updateLocationPostalCode postal_code model, Cmd.none )

        UpdateCurrentOfficerName name ->
            ( updateCurrentOfficerName name model, Cmd.none )

        UpdateCurrentOfficerEmail email ->
            ( updateCurrentOfficerEmail email model, Cmd.none )

        UpdateCurrentOfficerPhone phone ->
            ( updateCurrentOfficerPhone phone model, Cmd.none )

        StartRegistration ->
            ( { model | progress = EnteringBasicInfo }, Cmd.none )

        SaveCurrentOfficer index ->
            ( updateOfficerDict index model, Cmd.none )

        LoadOfficer index ->
            ( setOfficerToCurrent (getOfficer index model) model, Cmd.none )

        BackToBasicInfo ->
            ( { model | progress = EnteringBasicInfo }, Cmd.none )

        ReceivedInfo (Ok submission) ->
            ( { model | progress = FinishedRegistration, sharable_url = submission.sharable_url }, Cmd.none )

        ReceivedInfo (Err httpError) ->
            case httpError of
                Http.BadBody string ->
                    ( { model | name = string }, Cmd.none )

                _ ->
                    ( { model | name = "Some other Error" }, Cmd.none )

        SubmitRegistration ->
            ( model, sendItems model )


main : Program String Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \model -> Sub.none
        }


init : String -> ( Model, Cmd Msg )
init userToken =
    ( initialModel userToken, Cmd.none )



-- use array for this from feldman example , then save the officer form.
-- takes a list of officers and if there is a value ---
-- COMMANDS
--


zapierDecoder : Decoder Submission
zapierDecoder =
    succeed Submission
        |> required "status" string
        |> required "message" string
        |> required "id" string
        |> required "request_id" string
        |> required "shareable_url" string


sendItems : Model -> Cmd Msg
sendItems model =
    Http.request
        { method = "POST", headers = [ Http.header "Authorization" model.userToken ], timeout = Nothing, tracker = Nothing, url = phoenixApiUrl, body = Http.stringBody "application/json" (Enc.encode 3 (encodeLeagueLeague model)), expect = Http.expectJson ReceivedInfo zapierDecoder }


jsonBody model =
    Enc.encode 3 (encodeLeagueLeague model)



-- ENODING --


encodeLeagueLeague model =
    Enc.object [ ( "league", encodeLeague model ) ]


encodeLeague : Model -> Enc.Value
encodeLeague model =
    Enc.object
        [ ( "name", Enc.string model.name )
        , ( "status", Enc.string "URL" ) -- encodeLocation model.location )
        , ( "contacts", encodeOfficersDict model.officers )
        , ( "location", encodeLocation model.location )
        , ( "url", Enc.string "URL" )
        , ( "description", Enc.string "DESCRIPTION" )
        ]


encodeLocation : L.Location -> Enc.Value
encodeLocation location =
    Enc.object
        [ ( "address_line_1", Enc.string location.line1 )
        , ( "address_line_2", Enc.string location.line2 )
        , ( "city", Enc.string location.city )
        , ( "state", Enc.string location.state )
        , ( "postalCode", Enc.string location.postalCode )
        , ( "country", Enc.string "US" ) -- TODO add country option to the form and logic
        ]



--encodeOfficersDict : Dict Int Officer -> List ( String, Enc.Value )


encodeOfficersDict officers =
    Enc.list O.encode (officerList officers)


officerList officers =
    Dict.values officers


saveCurrentOfficer : Model -> O.Officer -> O.Officer
saveCurrentOfficer model officer =
    { officer
        | name = model.currentOfficer.name
        , email = model.currentOfficer.email
        , phone = model.currentOfficer.phone
        , position = model.currentOfficer.position
    }


checkRegistration : Model -> Model
checkRegistration model =
    case numberOfOfficers model > 0 of
        True ->
            { model | readyToSubmit = Complete }

        False ->
            model


numberOfOfficers : Model -> Int
numberOfOfficers model =
    Dict.size model.officers


updateOfficerDict : Int -> Model -> Model
updateOfficerDict index model =
    case okToSubmitOfficerForm model of
        True ->
            case Dict.member index model.officers of
                True ->
                    { model | officers = Dict.update index (Maybe.map (\officer -> saveCurrentOfficer model officer)) model.officers }
                        |> setOfficerToCurrent { id = numberOfOfficers model, name = "", email = "", phone = "", position = O.Other }
                        |> checkRegistration

                False ->
                    if numberOfOfficers model < maxOfficers then
                        { model | officers = Dict.insert (numberOfOfficers model) model.currentOfficer model.officers }
                            |> setOfficerToCurrent { id = numberOfOfficers model + 1, name = "", email = "", phone = "", position = O.Other }
                            |> checkRegistration

                    else
                        model

        False ->
            model


getOfficer : Int -> Model -> O.Officer
getOfficer index model =
    case Dict.get index model.officers of
        Just off ->
            off

        Nothing ->
            O.initialOfficer


setOfficerToCurrent : O.Officer -> Model -> Model
setOfficerToCurrent officer model =
    { model | currentOfficer = officer }



-- updates one aspect of the model at a time


updateLocation : String -> Model -> Model
updateLocation country ({ location } as model) =
    { model | location = { location | country = country } }


updateLocationLine1 : String -> Model -> Model
updateLocationLine1 line1 ({ location } as model) =
    { model | location = { location | line1 = line1, status = L.Initialized } }


updateLocationLine2 : String -> Model -> Model
updateLocationLine2 line2 ({ location } as model) =
    { model | location = { location | line2 = line2, status = L.Initialized } }


updateLocationCity : String -> Model -> Model
updateLocationCity city ({ location } as model) =
    { model | location = { location | city = city, status = L.Initialized } }


updateLocationState : String -> Model -> Model
updateLocationState state ({ location } as model) =
    { model | location = { location | state = state, status = L.Initialized } }


updateLocationPostalCode : String -> Model -> Model
updateLocationPostalCode postal_code ({ location } as model) =
    { model | location = { location | postalCode = postal_code, status = L.Initialized } }


updateCurrentOfficerName name ({ currentOfficer } as model) =
    { model | currentOfficer = { currentOfficer | name = name } }


updateCurrentOfficerEmail email ({ currentOfficer } as model) =
    { model | currentOfficer = { currentOfficer | email = email } }


updateCurrentOfficerPhone phone ({ currentOfficer } as model) =
    { model | currentOfficer = { currentOfficer | phone = phone } }


updateCurrentOfficerType : O.OfficerType -> Model -> Model
updateCurrentOfficerType officerType ({ currentOfficer } as model) =
    { model | currentOfficer = { currentOfficer | position = officerType } }


viewOfficerTypeChooser : O.OfficerType -> Html Msg
viewOfficerTypeChooser officerType =
    div [ class "form-check" ]
        [ input [ type_ "radio", name "officerType", class "form-check-input", onClick (ClickedOfficerType officerType) ] []
        , label [ class "form-check-label" ] [ text (O.officerTypeToString officerType) ]
        ]


viewHeader =
    div []
        [ header [ class "blog-header py-3" ]
            [ div [ class "row flex-nowrap justify-content-between align-items-center" ]
                [ div [ class "col-4 pt-1" ]
                    [-- a [ class "text-muted" ] [ text "Subscribe" ]
                    ]
                , div [ class "col-4 text-center" ]
                    [ a [ class "blog-header-logo text-dark" ] [ text "Registration System" ]
                    ]
                , div [ class "col-4 d-flex justify-content-end align-items-center" ]
                    [ a [ class "btn btn-sm btn-outline-secondary" ] [ text "Log in" ]
                    ]
                ]
            ]
        , div [ class "nav-scroller py-1 mb-2" ]
            [ nav [ class "nav d-flex justify-content-between" ]
                [ a [ class "p-2 text-muted" ] [ text "League" ]
                , a [ class "p-2 text-muted" ] [ text "Team" ]
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []
                , a [ class "p-2 text-muted" ] []

                --, a [ class "p-2 text-muted" ] [ text "Admin" ]
                ]
            ]
        ]


viewJumbotron model =
    case model.progress of
        NotStartedRegistration ->
            div [ classList [ ( "max-w-sm w-full lg:max-w-full lg:flex", True ) ], hidden False ]
                [ div [ class "col-md-6 px-0" ]
                    [ h1 [ class "display-4 font-italic" ] [ text "League Registration" ]
                    , p [ class "form-group" ]
                        [ div [ class "btn btn-secondary m-2" ]
                            [ a [ class "text-light", href "http://www.royhobbs.com/membership-and-insurance/" ] [ text "More Info..." ] ]
                        , button [ class "btn btn-primary m-2", onClick StartRegistration ] [ text "Start" ]
                        ]
                    ]
                ]

        _ ->
            text ""


viewProgressBar : Model -> Html Msg
viewProgressBar model =
    div [ class "flex justify-center m-5" ]
        [ statusBar model.progress
        ]



-- check the progress of the model, if model.progress == EnteringBasicInfo, then set the first button class to "btn-primary"


statusBar : Progress -> Html Msg
statusBar progress =
    case progress of
        EnteringBasicInfo ->
            div [ class "bg-gray-300 flex justify-center w-1/2" ]
                [ button [ class "w-1/3 ml-auto bg-blue-300 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Basic Information"
                    ]
                , button [ class "w-1/3 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Officers"
                    ]
                , button [ class "w-1/3 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Submit"
                    ]
                ]

        EnteringOfficerInfo ->
            div [ class "bg-gray-300 flex justify-center w-1/2" ]
                [ button [ class "w-1/3 ml-auto bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Basic Information"
                    ]
                , button [ class "w-1/3 bg-blue-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Officers"
                    ]
                , button [ class "w-1/3 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Submit"
                    ]
                ]

        FinishedRegistration ->
            div [ class "bg-gray-300 flex justify-center w-1/2" ]
                [ button [ class "w-1/3 ml-auto bg-blue-300 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Basic Information"
                    ]
                , button [ class "w-1/3 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Officers"
                    ]
                , button [ class "w-1/3 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" ]
                    [ text "Submit"
                    ]
                ]

        NotStartedRegistration ->
            text ""


viewAddressForm : Model -> Html Msg
viewAddressForm model =
    div []
        [ div [ class "flex flex-wrap -mx-3 mb-6" ]
            [ div [ class "w-full px-3" ]
                [ label [ for "inputAddress", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Address" ]
                , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white", id "inputAddress", placeholder "1234 Main St.", value model.location.line1, onInput UpdateLocationLine1 ] []
                ]
            ]
        , div [ class "flex flex-wrap -mx-3 mb-6" ]
            [ div [ class "w-full px-3" ]
                [ label [ for "inputAddress2", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Address 2" ]
                , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white", id "inputAddress2", placeholder "Apartment, Studio, or Floor", value model.location.line2, onInput UpdateLocationLine2 ] []
                ]
            ]
        , div [ class "flex flex-wrap -mx-3 mb-2" ]
            [ div [ class "w-full md:w-1/3 px-3 mb-6 md:mb-0" ]
                [ label [ for "inputCity", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "City" ]
                , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", id "inputCity", placeholder "City", value model.location.city, onInput UpdateLocationCity ] []
                ]
            , div [ class "w-full md:w-1/3 px-3 mb-6 md:mb-0" ]
                [ label [ for "inputState", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "State" ]
                , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", id "inputState", placeholder "State", value model.location.state, onInput UpdateLocationState ] []
                ]
            , div [ class "w-full md:w-1/3 px-3 mb-6 md:mb-0" ]
                [ label [ for "inputPostalCode", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Zip" ]
                , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", id "inputPostalCode", placeholder "Postal Code", value model.location.postalCode, onInput UpdateLocationPostalCode ] []
                ]
            ]
        ]


viewBasicInfoForm : Model -> Html Msg
viewBasicInfoForm model =
    case model.progress of
        EnteringBasicInfo ->
            Html.form [ class "w-1/2 mt-10", onSubmit SaveBasicInfo ]
                [ div [ class "flex flex-wrap -mx-3 mb-6" ]
                    [ div [ class "w-full md:w-1/2 px-3 mb-6 md:mb-0" ]
                        [ label [ for "inputLeagueName", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "League Name" ]
                        , input [ type_ "text", classList [ ( "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", True ) ], placeholder "League Name", id "inputLeagueName_", value model.name, onInput UpdateLeagueName ] []
                        ]
                    ]
                , viewAddressForm model
                , div [ class "flex justify-between mt-10" ]
                    [ button [ class "inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800 m-4" ] [ text "Cancel" ]
                    , button [ disabled False, class "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline m-4", type_ "submit" ] [ text "Next" ]
                    ]
                ]

        _ ->
            text ""


viewOfficerForm : Model -> Html Msg
viewOfficerForm model =
    case model.progress of
        EnteringOfficerInfo ->
            Html.form [ class "w-1/2 mt-10", onSubmit (SaveCurrentOfficer model.currentOfficer.id) ]
                -- onSubmit]
                [ div [ class "flex flex-wrap -mx-3 mb-6" ]
                    [ div [ class "w-full px-3" ]
                        [ label [ for "fullNameInput", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Full Name" ]
                        , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", placeholder "Full Name", id "fullNameInput", value model.currentOfficer.name, onInput UpdateCurrentOfficerName ] []
                        ]
                    ]
                , div [ class "flex flex-wrap -mx-3 mb-6" ]
                    [ div [ class "w-full px-3" ]
                        [ label [ for "emailInput", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Email" ]
                        , input [ type_ "email", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", placeholder "Email", id "emailInput", value model.currentOfficer.email, onInput UpdateCurrentOfficerEmail ] []
                        ]
                    ]
                , div [ class "flex flex-wrap -mx-3 mb-6" ]
                    [ div [ class "w-full px-3" ]
                        [ label [ for "phoneInput", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ] [ text "Phone" ]
                        , input [ type_ "text", class "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500", placeholder "Mobile Phone Number", id "phoneInput", value model.currentOfficer.phone, onInput UpdateCurrentOfficerPhone ] []
                        ]
                    ]
                , div [ class "flex flex-wrap -mx-3 mb-6" ]
                    [ div [ class "w-full px-3" ]
                        [ div [ class "text-gray-900 uppercase text-xs font-bold mb-2" ] [ text "Position" ]
                        , div [ id "choose-officer-type", class "block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" ]
                            (List.map viewOfficerTypeChooser [ O.Secretary, O.Other, O.President, O.Commissioner ])
                        ]
                    ]
                , div [ class "flex justify-between mt-10" ]
                    [ button [ class "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline m-4", onClick BackToBasicInfo ] [ text "Back" ]
                    , button [ classList [ ( "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline m-4", True ), ( "disabled", not (okToSubmitOfficerForm model) ) ], type_ "submit" ] [ text "Save" ]
                    ]
                ]

        _ ->
            text ""


len : String -> Int
len string =
    String.length string



-- Need to do better form validation here


okToSubmitOfficerForm : Model -> Bool
okToSubmitOfficerForm model =
    if (len model.currentOfficer.email >= 6) && (len model.currentOfficer.name >= 2) then
        True

    else
        False


viewOfficersInfo : Dict Int O.Officer -> Html Msg
viewOfficersInfo officers =
    div []
        (List.map viewOfficerInfo (Dict.values officers))


viewOfficerInfo : O.Officer -> Html Msg
viewOfficerInfo officer =
    p [ class "card-text", onClick (LoadOfficer officer.id) ]
        [ text (O.officerTypeToString officer.position ++ ": " ++ officer.name ++ " - " ++ officer.email)
        ]


viewInfoBox : Model -> Html Msg
viewInfoBox model =
    case model.progress of
        NotStartedRegistration ->
            text ""

        _ ->
            div [ class "flex w-1/2 mt-10 flex-col" ]
                [ div [ class "px-6", id "basic-info-card" ]
                    [ div [ class "font-bold text-xl mb-2" ]
                        [ text ("General Info for " ++ model.name)
                        ]
                    , div [ class "card-body" ]
                        [ div [ class "font-bold text-l mb-2" ]
                            [ text "Location"
                            ]
                        , p [ class "text-gray-700 text-base mb-5" ]
                            [ L.viewLocation model.location ]
                        , div [ class "font-bold text-l mb-2" ]
                            [ text "Officers"
                            ]
                        , viewOfficersInfo model.officers
                        ]
                    ]
                , viewSubmitRegistration model
                ]



-- Short Description : takes a model, branches on success and shows a message saying thank you


viewSuccessMessage : Model -> Html Msg
viewSuccessMessage model =
    case model.progress of
        FinishedRegistration ->
            div [ class "border rounded p-3 mb-3 ml-3 col-md-6" ]
                [ h5 [ class "card-title" ] [ text "Thank you for registering with Roy Hobbs!" ]
                , div [ class "card-body" ] [ text ("Share this with your teams: " ++ model.sharable_url) ]
                ]

        _ ->
            text ""


viewSubmitRegistration model =
    case model.progress of
        FinishedRegistration ->
            text ""

        _ ->
            case model.readyToSubmit of
                Complete ->
                    button [ class "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline m-4", onClick SubmitRegistration ] [ text "Submit Registration" ]

                NotComplete ->
                    text ""


viewFooter model =
    footer [ class "flex justify-start flex-col mt-40" ]
        [ p [ class "text-gray-800 text-center" ]
            [ text "Contact Us - "
            , a [ href "mailto:support@royhobbs.com" ] [ text "Email" ]
            , text " - Call : "
            , a [ href "tel:13309233400" ] [ text "1-330-923-3400" ]
            ]
        , p [ class "text-gray-800 text-center" ]
            [ text "© 2019 Roy Hobbs Baseball"
            ]
        , p [ class "text-gray-800 text-center" ]
            [ text ("userToken available in LeagueRegistration: " ++ model.userToken) ]
        ]


view : Model -> Html Msg
view model =
    div [ class "flex container mx-auto content-center flex-col" ]
        [ viewJumbotron model
        , viewProgressBar model
        , div [ class "flex px-2 flex-row" ]
            [ viewSuccessMessage model
            , viewBasicInfoForm model
            , viewOfficerForm model
            , viewInfoBox model
            ]
        , viewFooter model
        ]
