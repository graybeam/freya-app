module Leader exposing (Leader, LeaderType(..), encode, initialLeader, leaderTypeToString)

import Json.Encode as E


type LeaderType
    = Manager
    | Stats
    | Secretary
    | Other


type alias Leader =
    { id : Int
    , name : String
    , email : String
    , phone : String
    , position : LeaderType
    }


initialLeader : Leader
initialLeader =
    { id = 0
    , name = ""
    , email = ""
    , phone = ""
    , position = Manager
    }


encode : Leader -> E.Value
encode leader =
    E.object
        [ ( "name", E.string leader.name )
        , ( "email", E.string leader.email )
        , ( "phone", E.string leader.phone )
        , ( "role", E.string (leaderTypeToString leader.position) ) -- TODO match up positions with the API
        ]


leaderTypeToString : LeaderType -> String
leaderTypeToString leaderType =
    case leaderType of
        Manager ->
            "Manager"

        Stats ->
            "Stats"

        Secretary ->
            "Secretary"

        Other ->
            "Other"
