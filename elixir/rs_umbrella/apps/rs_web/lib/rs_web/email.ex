defmodule RsWeb.Email do
  use Bamboo.Phoenix, view: RsWeb.EmailView

  def registration_email(user) do
    base_email()
    |> IO.inspect(label: "before user #{inspect(user)}")
    |> to([user.credential.email])
    |> subject("Thank you for Registering")
    |> render(:registration_welcome, user: user)
  end

  defp base_email() do
    new_email()
    |> from("Registration System<mchambers@royhobbs.com>")
    |> put_header("Reply-To", "teammatesupport@royhobbs.com")
    |> put_html_layout({RsWeb.LayoutView, "email.html"})
    |> put_text_layout({RsWeb.LayoutView, "email.text"})
  end

end
