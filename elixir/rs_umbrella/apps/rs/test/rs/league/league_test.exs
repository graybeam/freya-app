defmodule Rs.LeagueTest do
  use RsWeb.DataCase

  alias Rs.League

  describe "division_types" do
    alias Rs.League.DivisionType

    @valid_attrs %{division: "some division", title: "some title"}
    @update_attrs %{division: "some updated division", title: "some updated title"}
    @invalid_attrs %{division: nil, title: nil}

    def division_type_fixture(attrs \\ %{}) do
      {:ok, division_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> League.create_division_type()

      division_type
    end

    test "list_division_types/0 returns all division_types" do
      division_type = division_type_fixture()
      assert League.list_division_types() == [division_type]
    end

    test "get_division_type!/1 returns the division_type with given id" do
      division_type = division_type_fixture()
      assert League.get_division_type!(division_type.id) == division_type
    end

    test "create_division_type/1 with valid data creates a division_type" do
      assert {:ok, %DivisionType{} = division_type} = League.create_division_type(@valid_attrs)
      assert division_type.division == "some division"
      assert division_type.title == "some title"
    end

    test "create_division_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = League.create_division_type(@invalid_attrs)
    end

    test "update_division_type/2 with valid data updates the division_type" do
      division_type = division_type_fixture()
      assert {:ok, %DivisionType{} = division_type} = League.update_division_type(division_type, @update_attrs)
      assert division_type.division == "some updated division"
      assert division_type.title == "some updated title"
    end

    test "update_division_type/2 with invalid data returns error changeset" do
      division_type = division_type_fixture()
      assert {:error, %Ecto.Changeset{}} = League.update_division_type(division_type, @invalid_attrs)
      assert division_type == League.get_division_type!(division_type.id)
    end

    test "delete_division_type/1 deletes the division_type" do
      division_type = division_type_fixture()
      assert {:ok, %DivisionType{}} = League.delete_division_type(division_type)
      assert_raise Ecto.NoResultsError, fn -> League.get_division_type!(division_type.id) end
    end

    test "change_division_type/1 returns a division_type changeset" do
      division_type = division_type_fixture()
      assert %Ecto.Changeset{} = League.change_division_type(division_type)
    end
  end
end
