port module Main exposing (main)

--import Json.Decode as Decode exposing(..)
--import Json.Encode as Encode exposing (..)

import Browser
import Html exposing (Attribute, Html, a, button, div, h1, header, input, li, nav, option, p, pre, select, text, ul)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import Http
import Json.Decode exposing (Decoder, field, int, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import String


urlPrefix : String
urlPrefix =
    "http://localhost:4000/"


leagueApiV1 : String
leagueApiV1 =
    "api/v1/leagues/6"


type alias Model =
    { flags : String
    , status : String
    , serverString : League
    }


type alias JsonContact =
    { id : Int
    , name : String
    , email : String
    , phone : String
    , position : String
    }


type alias League =
    { id : Int
    , title : String
    , url : String
    , slug : String
    }


type alias Contact =
    { id : String
    , name : String
    , contact_type : String
    }


type alias Data =
    { data : String
    }


type alias Message =
    { id : String
    , message : String
    , request_id : String
    , status : String
    }


okDecoder : Decoder Message
okDecoder =
    succeed Message
        |> required "id" string
        |> required "message" string
        |> required "request_id" string
        |> required "status" string


testDecoder : Decoder League
testDecoder =
    field "data" leagueDecoder


leagueDecoder : Decoder League
leagueDecoder =
    succeed League
        |> required "id" int
        |> required "title" string
        |> required "slug" string
        |> optional "url" string "(not supplied)"


contactDecoder : Decoder Contact
contactDecoder =
    succeed Contact
        |> required "id" string
        |> required "name" string
        |> required "contact_type" string


type Msg
    = LetsGo
    | GotLeagues (Result Http.Error League)


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : String -> ( Model, Cmd Msg )
init flags =
    ( { flags = flags, status = "Initialized", serverString = { id = 0, title = "blank", url = "http://localhost", slug = "STATUS" } }, initialCommandRequest flags )


initialCommand : Cmd Msg
initialCommand =
    Http.get { url = urlPrefix ++ leagueApiV1, expect = Http.expectJson GotLeagues testDecoder }


initialCommandRequest : String -> Cmd Msg
initialCommandRequest flags =
    let
        request_auth =
            Http.request
                { method = "GET"
                , headers = [ Http.header "Accept" "application/json", Http.header "Content-Type" "application/json", Http.header "Authorization" flags ]
                , url = urlPrefix ++ leagueApiV1
                , body = Http.emptyBody
                , expect = Http.expectJson GotLeagues testDecoder
                , timeout = Nothing
                , tracker = Nothing
                }
    in
    request_auth


update msg model =
    case msg of
        LetsGo ->
            ( model, Cmd.none )

        GotLeagues result ->
            case result of
                Ok response ->
                    ( { model | serverString = response }, Cmd.none )

                Err httpError ->
                    case httpError of
                        Http.BadBody string ->
                            ( { model | status = string }, Cmd.none )

                        _ ->
                            ( { model | status = "Some unknown error" }, Cmd.none )


subscriptions model =
    Sub.none


leagueToString : League -> String
leagueToString league =
    "Title: " ++ league.title ++ " Url: " ++ league.url ++ " ID: " ++ String.fromInt league.id


okMessageToString : Message -> String
okMessageToString okMessage =
    okMessage.id ++ " " ++ okMessage.message ++ " " ++ okMessage.request_id ++ " ID: " ++ okMessage.status


viewPhoenixToken : Model -> String -> Html Msg
viewPhoenixToken model args =
    div [ class "card" ]
        [ div [ class "card-header" ]
            [ text "Phoenix JSInterop Debug" ]
        , ul [ class "list-group list-group-flush" ]
            [ li [ class "list-group-item" ] [ text ("Phoenix Auth Token: " ++ model.flags) ]
            , li [ class "list-group-item" ] [ text ("Channel Data: " ++ model.status) ]
            , li [ class "list-group-item" ] [ text ("League Data String: " ++ leagueToString model.serverString) ]
            , li [ class "list-group-item" ] [ text ("Args: " ++ args) ]
            ]
        ]


view model =
    div [ class "container mt-5" ]
        [ viewPhoenixToken model "Args" ]
