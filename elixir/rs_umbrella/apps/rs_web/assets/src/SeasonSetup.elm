module SeasonSetup exposing (League)

import Date exposing (..)
import Time exposing (..)


type alias League =
    { name : String
    , id : Int
    }


type alias Season =
    { name : String
    , league : League
    , startDate : Date
    , endDate : Date
    }


todaysDate =
    Date.fromCalendarDate 2019 May 13


initStartDate =
    todaysDate


initEndDate =
    Date.add Months 3 initStartDate


initSeason =
    { name = "", league = { name = "Mac's League", id = 3 }, startDate = initStartDate, endDate = initEndDate }


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : String -> ( Model, Cmd Msg )
init flags =
    ( { flags = flags, status = "Initialized", serverString = { id = 0, title = "blank", url = "http://localhost", slug = "STATUS" } }, initialCommandRequest flags )
