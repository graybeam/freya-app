defmodule RsWeb.API.V1.LeagueView do
  use RsWeb, :view
  require Logger
  import Rs.Organization.League

  def render("index.json", %{leagues: leagues}) do
    %{data: render_many(leagues, __MODULE__, "league_with_contacts.json")}
  end

  def render("show.json", %{league: league}) do
    %{data: render_one(league, __MODULE__, "league_with_contacts.json")}
  end

  def render("ok.json", %{league: league}) do
    %{status: "ok", message: "League #{league.name} registered successfully!", id: "#{league.id}", request_id: "request_id", shareable_url: "#{league.slug}"}
  end

  def render(conn, "new.json", %{changeset: changeset}) do
    %{status: "error", message: "changeset #{inspect(changeset)}", id: "13131313", request_id: "13131313"}  
  end
  def render("league_with_contacts.json", %{league: league}) do

    %{
      type: "league",
      id: league.id,
      title: league.name,
      url: league.url,
      slug: league.slug,
      contacts: render_many(league.contacts, RsWeb.API.V1.ContactView, "contact.json")
    }
  end
end
