defmodule RsWeb.LinkController do
  use RsWeb, :controller

  alias Rs.Organization
  alias Rs.Organization.Link

  action_fallback RsWeb.FallbackController

  def index(conn, _params) do
    links = Organization.list_links()
    render(conn, "index.json", links: links)
  end

  def create(conn, %{"link" => link_params}) do
    with {:ok, %Link{} = link} <- Organization.create_link(link_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.link_path(conn, :show, link))
      |> render("show.json", link: link)
    end
  end

  def show(conn, %{"id" => id}) do
    link = Organization.get_link!(id)
    render(conn, "show.json", link: link)
  end

  def update(conn, %{"id" => id, "link" => link_params}) do
    link = Organization.get_link!(id)

    with {:ok, %Link{} = link} <- Organization.update_link(link, link_params) do
      render(conn, "show.json", link: link)
    end
  end

  def delete(conn, %{"id" => id}) do
    link = Organization.get_link!(id)

    with {:ok, %Link{}} <- Organization.delete_link(link) do
      send_resp(conn, :no_content, "")
    end
  end

  def get_and_redirect(conn, %{"id" => id}) do
    url = id
    |> Organization.get_link!()
    |> Map.get(:url)
    redirect(conn, external: url)
  end

end
