defmodule Rs.RegistrationTest do
  use RsWeb.DataCase

  alias Rs.Registration

  describe "players" do
    alias Rs.Registration.Player

    @valid_attrs %{email: "some email", name: "some name"}
    @update_attrs %{email: "some updated email", name: "some updated name"}
    @invalid_attrs %{email: nil, name: nil}

    def player_fixture(attrs \\ %{}) do
      {:ok, player} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Registration.create_player()

      player
    end

    test "list_players/0 returns all players" do
      player = player_fixture()
      assert Registration.list_players() == [player]
    end

    test "get_player!/1 returns the player with given id" do
      player = player_fixture()
      assert Registration.get_player!(player.id) == player
    end

    test "create_player/1 with valid data creates a player" do
      assert {:ok, %Player{} = player} = Registration.create_player(@valid_attrs)
      assert player.email == "some email"
      assert player.name == "some name"
    end

    test "create_player/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Registration.create_player(@invalid_attrs)
    end

    test "update_player/2 with valid data updates the player" do
      player = player_fixture()
      assert {:ok, %Player{} = player} = Registration.update_player(player, @update_attrs)
      assert player.email == "some updated email"
      assert player.name == "some updated name"
    end

    test "update_player/2 with invalid data returns error changeset" do
      player = player_fixture()
      assert {:error, %Ecto.Changeset{}} = Registration.update_player(player, @invalid_attrs)
      assert player == Registration.get_player!(player.id)
    end

    test "delete_player/1 deletes the player" do
      player = player_fixture()
      assert {:ok, %Player{}} = Registration.delete_player(player)
      assert_raise Ecto.NoResultsError, fn -> Registration.get_player!(player.id) end
    end

    test "change_player/1 returns a player changeset" do
      player = player_fixture()
      assert %Ecto.Changeset{} = Registration.change_player(player)
    end
  end
end
