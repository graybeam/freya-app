defmodule Rs.League do
  @moduledoc """
  The League context.
  """

  import Ecto.Query, warn: false
  alias Rs.Repo

  alias Rs.League.DivisionType

  @doc """
  Returns the list of division_types.

  ## Examples

      iex> list_division_types()
      [%DivisionType{}, ...]

  """
  def list_division_types do
    Repo.all(DivisionType)
  end

  @doc """
  Gets a single division_type.

  Raises `Ecto.NoResultsError` if the Division type does not exist.

  ## Examples

      iex> get_division_type!(123)
      %DivisionType{}

      iex> get_division_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_division_type!(id), do: Repo.get!(DivisionType, id)

  @doc """
  Creates a division_type.

  ## Examples

      iex> create_division_type(%{field: value})
      {:ok, %DivisionType{}}

      iex> create_division_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_division_type(attrs \\ %{}) do
    %DivisionType{}
    |> DivisionType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a division_type.

  ## Examples

      iex> update_division_type(division_type, %{field: new_value})
      {:ok, %DivisionType{}}

      iex> update_division_type(division_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_division_type(%DivisionType{} = division_type, attrs) do
    division_type
    |> DivisionType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a DivisionType.

  ## Examples

      iex> delete_division_type(division_type)
      {:ok, %DivisionType{}}

      iex> delete_division_type(division_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_division_type(%DivisionType{} = division_type) do
    Repo.delete(division_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking division_type changes.

  ## Examples

      iex> change_division_type(division_type)
      %Ecto.Changeset{source: %DivisionType{}}

  """
  def change_division_type(%DivisionType{} = division_type) do
    DivisionType.changeset(division_type, %{})
  end
end
