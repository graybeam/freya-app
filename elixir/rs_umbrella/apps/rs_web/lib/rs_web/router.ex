defmodule RsWeb.Router do
  use RsWeb, :router
  require Logger

  if Mix.env == :dev do
   forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug RsWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug RsWeb.API.V1.Auth
    #plug RsWeb.Plug.ConnInterceptor
    # plug :token_authentication
    # plug :dispatch
  end

  pipeline :graphql do
  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json, Absinthe.PlugParser],
    pass: ["*/*"],
    json_decoder: Jason
  end
  #
  #pipeline :graphql do
   # plug Absinthe.Plug,
    #  schema: RsWeb.Schema,
#

  #json_codec: Jason
  #end
  scope "/", RsWeb do
    pipe_through [:browser, :authenticate_user]
    get "/", PageController, :index
  end
  scope "/", RsWeb do
    pipe_through [:browser]

    #get "/league/register/:id", LeagueRegController, :register
    #get "/league/:messenger", LeagueRegController, :show
    get "/team", TeamRegController, :index

    resources "/users", UserController, only: [:index, :show, :new, :create]
    resources "/sessions", SessionController, only: [:new, :create, :delete]

    get "/visit/:id", VisitController, :show

    #:id is for looking up the league
  end

  scope "/league", RsWeb do
    pipe_through [:browser,:authenticate_user]

    get "/register", LeagueRegController, :new
    get "/register/:id", LeagueRegController, :register

  end

  scope "/manage", RsWeb do
    pipe_through [:browser, :authenticate_user]

    resources "/leagues", LeagueController
    resources "/teams", TeamController
  end

  scope "/links", RsWeb do
    get "/:id", LinkController, :get_and_redirect
  end

  # Other scopes may use custom stacks.
  scope "/api/v1", RsWeb.API.V1 do
    pipe_through :api # :authenticate_user_from_token]
    # post "/leagues", LeagueController, :create
    resources "/leagues", LeagueController, only: [:index, :show, :create]
  end


  scope path: "/graphql" do
    # TODO using this context in assigns conn.private[:absinthe][:context] [:authenticate_user_from_token]
    pipe_through :graphql

    forward "/graphiql", Absinthe.Plug.GraphiQL,
        schema: RsWeb.Schema, json_codec: Jason
    forward "/graphql",
        Absinthe.Plug,
        schema: RsWeb.Schema, json_codec: Jason
   end

  scope "/test/api", RsWeb do
    pipe_through :api
    resources "/leagues", LeagueRegController

  end
end
