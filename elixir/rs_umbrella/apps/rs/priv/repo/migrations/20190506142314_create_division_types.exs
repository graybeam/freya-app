defmodule Rs.Repo.Migrations.CreateDivisionTypes do
  use Ecto.Migration

  def change do
    create table(:division_types) do
      add :title, :string
      add :division, :string

      timestamps()
    end

  end
end
