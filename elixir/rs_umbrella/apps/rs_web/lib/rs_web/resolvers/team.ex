defmodule RsWeb.Resolvers.Team do
  alias Rs.Accounts.User
  alias Rs.Organization.{League, Team}

  def find_team(_parent, %{id: id}, _resolution) do
    team = Organization.get_team!(id)
    {:ok, team}
  end

  def list_teams(%League{} = league, _args, _resolution) do
    {:ok, Organization.list_league_teams(league)}
  end


end