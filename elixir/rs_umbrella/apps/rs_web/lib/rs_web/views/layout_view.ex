defmodule RsWeb.LayoutView do
  use RsWeb, :view

  def list_leagues(current_user) do
    leagues = Rs.Organization.list_user_leagues(current_user)
    {:ok, length(leagues), leagues}
  end

  def list_teams(current_user) do
    teams = Rs.Organization.list_user_teams(current_user)
    {:ok, length(teams),teams}
  end
end
