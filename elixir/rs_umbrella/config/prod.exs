use Mix.Config

# Do not print debug messages in production
config :logger, level: :info

config :rs_umbrella, RsWeb.Endpoint,
       http: [:inet6, port: System.get_env("PORT") || 4000],
       url: [host: "registration.royhobbs.com", port: 80],
       cache_static_manifest: "priv/static/cache_manifest.json",
       server: true,
       code_reloader: false