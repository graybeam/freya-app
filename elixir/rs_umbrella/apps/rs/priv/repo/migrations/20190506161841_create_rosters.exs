defmodule Rs.Repo.Migrations.CreateRosters do
  use Ecto.Migration

  def change do
    create table(:rosters) do
      add :title, :string
      add :context, :string
      add :team_id, references(:teams, on_delete: :nothing)

      timestamps()
    end

    create index(:rosters, [:team_id])
  end
end
