module Units exposing (..)
type Length units =
    Length Float units

 type Cm =
    Cm
 type In =
    In

 cm : Float -> Length Cm
 inches : Float -> Length In

 --phantom type
-- add : Length units -> Length units -> Length units
-- add (Length num1) (Length num2) =
--    Length (num1 + num2)

  add : Length units -> Length units -> Length units
  add (Length num1 units1) (Length num2 units2) =
     Length (num1 + num2) units