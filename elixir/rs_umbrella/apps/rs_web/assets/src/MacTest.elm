module MacTest exposing (League, Location, Model, Officer, OfficerType(..), Progress(..), User, initialLocation, initialModel, initialOfficer, officers, users)

import Dict exposing (Dict)


type OfficerType
    = Secretary
    | President
    | Commissioner
    | Other


type alias Officer =
    { id : Int
    , name : String
    , email : String
    , phone : String
    , position : OfficerType
    }


type alias Location =
    { line1 : String
    , line2 : String
    , city : String
    , state : String
    , postalCode : String
    , country : String
    }


type alias League =
    { name : String
    , officers : List Officer
    , location : Location
    }


type Progress
    = EnteringBasicInfo
    | EnteringOfficerInfo
    | FinishedRegistration
    | NotStartedRegistration


type alias Model =
    { name : String
    , location : Location
    , officers : List Officer
    , progress : Progress
    , currentOfficer : Officer
    , primaryOfficer : Officer
    , secondaryOfficer : Officer
    , tertiaryOfficer : Officer
    }


initialLocation : Location
initialLocation =
    { line1 = "Address Line 1"
    , line2 = "Address Line 2"
    , city = "Cedar Park"
    , state = "Texas"
    , postalCode = "78613"
    , country = "United States"
    }


initialOfficer : Officer
initialOfficer =
    { id = 0
    , name = ""
    , email = ""
    , phone = ""
    , position = Commissioner
    }


initialModel : Model
initialModel =
    { officers = [ initialOfficer ]
    , location = initialLocation
    , name = "Default Name"
    , progress = NotStartedRegistration
    , currentOfficer = initialOfficer
    , primaryOfficer = initialOfficer
    , secondaryOfficer = initialOfficer
    , tertiaryOfficer = initialOfficer
    }


users : Dict String User
users =
    Dict.fromList
        [ ( "Alice", User "Alice" 28 1.65 )
        , ( "Bob", User "Bob" 19 1.82 )
        , ( "Chuck", User "Chuck" 33 1.75 )
        ]


type alias User =
    { name : String
    , age : Int
    , height : Float
    }


officers : Dict Int Officer
officers =
    Dict.fromList
        [ ( 1, Officer 0 "" "" "" Secretary )
        , ( 2, Officer 0 "" "" "" Commissioner )
        , ( 3, Officer 0 "" "" "" Other )
        ]
