// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

import "bootstrap"
// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

//import { Elm } from "../src/Main.elm";
import { Elm as ElmMain } from "../src/Main.elm";
import { Elm as ElmLeagueRegistration } from "../src/LeagueRegistration.elm";
import { Elm as ElmTeam } from "../src/TeamRegistration.elm";
import { Elm as ElmLeague } from "../src/League.elm";

import socket from "./socket"
import League from "./league"
//import Team from "./team"

League.init(socket, document.getElementById("league"))

//var app1 = ElmMain.Main.init({ node: document.getElementById('elm-main') });
//var app2 = ElmL.LeagueReg.init({ node: document.getElementById('elm-league-main')});

var chooseElmApp = function(nodeName, modName){
  var element = document.getElementById(nodeName);
  if(typeof(element) != 'undefined' && element !== null)
  {
    let flags = window.userToken
    switch(nodeName) {
      case "elm-main":
          return modName.Main.init({ node: element, flags: flags });
      case "elm-league-registration-main":
          return modName.LeagueRegistration.init({ node: element, flags: flags});
      case "elm-team-main":
          return modName.TeamRegistration.init({ node: element, flags: flags});
    }

  }
};

var appMain = chooseElmApp("elm-main", ElmMain);
const elmDiv = document.getElementById('elm-league');
if(elmDiv) {
    const appLeague = ElmLeague.LeaguePage.init({ node: elmDiv, flags: "poo"});
}
var appLeagueRegistration = chooseElmApp("elm-league-registration-main", ElmLeagueRegistration);
var appTeam = chooseElmApp("elm-team-main", ElmTeam);



let league = document.getElementById("league");


if(league) {
  console.log("channel ready!")
}
