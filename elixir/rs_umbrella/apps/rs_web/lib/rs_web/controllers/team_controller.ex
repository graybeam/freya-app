defmodule RsWeb.TeamController do
  use RsWeb, :controller

  alias Rs.Organization
  alias Rs.Organization.Team

  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.current_user]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params, current_user) do
    teams = Organization.list_user_teams(current_user)
    render(conn, "index.html", teams: teams)
  end

  def new(conn, _params, current_user) do
    changeset = Organization.change_team(%Team{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"team" => team_params}, current_user) do
    case Organization.create_team(current_user, team_params) do
      {:ok, team} ->
        conn
        |> put_flash(:info, "Team created successfully.")
        |> redirect(to: Routes.team_path(conn, :show, team))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id},current_user) do

    team = Organization.get_user_team!(current_user, String.to_integer(id))
    render(conn, "show.html", team: team)
  end

  def edit(conn, %{"id" => id}, current_user) do
    team = Organization.get_user_team!(current_user, id)
    changeset = Organization.change_team(team)
    render(conn, "edit.html", team: team, changeset: changeset)
  end

  def update(conn, %{"id" => id, "team" => team_params}, current_user) do
    team = Organization.get_team!(id)

    case Organization.update_team(team, team_params) do
      {:ok, team} ->
        conn
        |> put_flash(:info, "Team updated successfully.")
        |> redirect(to: Routes.team_path(conn, :show, team))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", team: team, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id},current_user) do
    team = Organization.get_team!(id)
    {:ok, _team} = Organization.delete_team(team)

    conn
    |> put_flash(:info, "Team deleted successfully.")
    |> redirect(to: Routes.team_path(conn, :index))
  end
end
