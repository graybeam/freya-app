defmodule Rs.OrganizationTest do
  use RsWeb.DataCase

  alias Rs.Organization

  describe "teams" do
    alias Rs.Organization.Team

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def team_fixture(attrs \\ %{}) do
      {:ok, team} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_team()

      team
    end

    test "list_teams/0 returns all teams" do
      team = team_fixture()
      assert Organization.list_teams() == [team]
    end

    test "get_team!/1 returns the team with given id" do
      team = team_fixture()
      assert Organization.get_team!(team.id) == team
    end

    test "create_team/1 with valid data creates a team" do
      assert {:ok, %Team{} = team} = Organization.create_team(@valid_attrs)
      assert team.name == "some name"
    end

    test "create_team/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_team(@invalid_attrs)
    end

    test "update_team/2 with valid data updates the team" do
      team = team_fixture()
      assert {:ok, %Team{} = team} = Organization.update_team(team, @update_attrs)
      assert teaam.name == "some updated name"
    end

    test "update_teaam/2 with invalid data returns error changeset" do
      teaam = teaam_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_teaam(teaam, @invalid_attrs)
      assert teaam == Organization.get_teaam!(teaam.id)
    end

    test "delete_teaam/1 deletes the teaam" do
      teaam = teaam_fixture()
      assert {:ok, %Teaam{}} = Organization.delete_teaam(teaam)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_teaam!(teaam.id) end
    end

    test "change_teaam/1 returns a teaam changeset" do
      teaam = teaam_fixture()
      assert %Ecto.Changeset{} = Organization.change_teaam(teaam)
    end
  end

  describe "links" do
    alias Rs.Organization.Link

    @valid_attrs %{hash: "some hash", url: "some url"}
    @update_attrs %{hash: "some updated hash", url: "some updated url"}
    @invalid_attrs %{hash: nil, url: nil}

    def link_fixture(attrs \\ %{}) do
      {:ok, link} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_link()

      link
    end

    test "list_links/0 returns all links" do
      link = link_fixture()
      assert Organization.list_links() == [link]
    end

    test "get_link!/1 returns the link with given id" do
      link = link_fixture()
      assert Organization.get_link!(link.id) == link
    end

    test "create_link/1 with valid data creates a link" do
      assert {:ok, %Link{} = link} = Organization.create_link(@valid_attrs)
      assert link.hash == "some hash"
      assert link.url == "some url"
    end

    test "create_link/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_link(@invalid_attrs)
    end

    test "update_link/2 with valid data updates the link" do
      link = link_fixture()
      assert {:ok, %Link{} = link} = Organization.update_link(link, @update_attrs)
      assert link.hash == "some updated hash"
      assert link.url == "some updated url"
    end

    test "update_link/2 with invalid data returns error changeset" do
      link = link_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_link(link, @invalid_attrs)
      assert link == Organization.get_link!(link.id)
    end

    test "delete_link/1 deletes the link" do
      link = link_fixture()
      assert {:ok, %Link{}} = Organization.delete_link(link)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_link!(link.id) end
    end

    test "change_link/1 returns a link changeset" do
      link = link_fixture()
      assert %Ecto.Changeset{} = Organization.change_link(link)
    end
  end
end
