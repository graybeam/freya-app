defmodule Rs.Organization.League do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rs.Organization.Contact

  @primary_key {:id, Rs.Organization.Permalink, autogenerate: true}
  schema "leagues" do
    field :description, :string
    field :location, :string
    field :name, :string
    field :url, :string
    field :slug, :string
    field :status, :string

    belongs_to :user, Rs.Accounts.User
    belongs_to :category, Rs.Organization.Category
    has_many :annotations, Rs.Organization.Annotation
    has_many :contacts, Rs.Organization.Contact

    timestamps()
  end

  @doc false
  def changeset(league, attrs) do
    league
    |> cast(attrs, [:url, :name, :description, :category_id])
    |> validate_required([:url, :name, :description])
    |> IO.inspect(label: "before passed to contacts")
    |> cast_assoc(:contacts, with: &Contact.changeset/2)
    |> assoc_constraint(:category)
    |> slugify_title()
  end

  defp slugify_title(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  # TODO - Add a place for a magic registration link here, slugify might work for now
  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
  end
end
