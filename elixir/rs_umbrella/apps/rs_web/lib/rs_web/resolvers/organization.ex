defmodule RsWeb.Resolvers.Organization do
  alias Rs.Organization

  def find_link(_parent, %{hash: hash}, _resolution) do
    IO.inspect(hash)
    link = Organization.get_link!(hash)

    {:ok, link}
  end

  def create_link(_parent, args) do
    IO.inspect("inside create_link and #{inspect args}")
    Rs.Organization.create_link(args.arguments)
  end
end