defmodule RsWeb.LeagueRegView do
  use RsWeb, :view
  alias RsWeb.League
  require Logger

  def render("show.json", %{"league" => league_name, "description" => description}) do
    Logger.info("In render(show.json): #{inspect(description)}")
    %{data: %RsWeb.League{name: league_name, description: description}}
  end
end
