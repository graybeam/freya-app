defmodule RsWeb.API.V1.ContactView do
  use RsWeb, :view
  require Logger

  def render("contact.json", %{contact: contact}) do
    Logger.debug("#{inspect(contact)}")

    %{
      type: "contact",
      id: contact.id,
      name: contact.name
    }
  end
end
