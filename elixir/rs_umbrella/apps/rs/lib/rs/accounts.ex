defmodule Rs.Accounts do
  @moduledoc """
  The accounts context
  """
  alias Rs.Repo
  alias Rs.Accounts.User
  import Ecto.Query


  @doc """
  Find the email from the user login process and if the user doesnt exist, create user with the given email
  TODO: create worker to remove old magic links (older than 2 hours?)
  Accounts.register_user(%{credential: %{email: "mac@royhobbs.com", password: "hutensahtnu"}, name: "Poop", username: "mac@royhobbs.com"})

  Changeset Struct
   #Ecto.Changeset<
   action: :insert,
   changes: %{
     credential: #Ecto.Changeset<
       action: :insert,
       changes: %{
         email: "mac@royhobbs.com",
         password: "hutensahtnu",
         password_hash: "$pbkdf2-sha512$160000$.Jl2ky2ZUyELIW.i4gGS/Q$qJ5ztj0DO3/NOW4ZhTmnOZlxPKGYR3aUV4qCjH42n5YwB2JJL.oTqj9PAgyWFW7.TePMcrXWqkQS0w6XKL2oBg"
       },
       errors: [],
       data: #Rs.Accounts.Credential<>,
       valid?: true
     >,
     name: "mac@royhobbs.com",
     username: "mac@royhobbs.com"
   },
   errors: [
     username: {"has already been taken",
      [constraint: :unique, constraint_name: "users_username_index"]}
   ],
   data: #Rs.Accounts.User<>,
   valid?: false
  >

  1. create login token for the given email.
  2. get user id or create user and get the user id.
  3. save the token and the user id with a expiration date of 2 hours + current time
  4. send an email to email with the list

  token = Phoenix.Token.sign(RsWeb.Endpoint, "salty dog", user.id)
  "SFMyNTY.g3QAAAACZAAEZGF0YWEGZAAGc2lnbmVkbgYAhedHQ2sB.Sclgu9f7G0Q5HXl7GvFSOGszYtHLimDHE83JkBg3A2M"
  iex(21)> Phoenix.Token.verify(RsWeb.Endpoint, "salty dog", token, max_age: 86400)
  {:ok, 6}
  iex(22)> token = Phoenix.Token.sign(RsWeb.Endpoint, "salty dog", {user.id,"sibil"})
  "SFMyNTY.g3QAAAACZAAEZGF0YWgCYQZtAAAABXNpYmlsZAAGc2lnbmVkbgYAm1pIQ2sB.8d0_UQ1Fhkb0Y3HqNBoZpP5GsHReXtnQeVK_TG6yYrY"
  iex(23)> Phoenix.Token.verify(RsWeb.Endpoint, "salty dog", token, max_age: 86400)
  {:ok, {6, "sibil"}}
  """
  @spec create_magic_link_login(email :: String.t()) :: any
  def create_magic_link_login(email) when is_binary(email) do
    user  = get_user_by_email(email)
    save_user = case user do
      nil ->
        Accounts.register_user(%{credential: %{email: email, password: "#{email}"}, name: email, username: email}) #register the user
      _ ->
        user
    end

    #create magic login token
    #save magic login  save the token and the user id with the current time + 2 hours shifted = Timex.shift(Timex.now, hours: 2)

  end

  def get_user_by_email(email) do
    from(u in User, join: c in assoc(u, :credential), where: c.email == ^email)
    |> Repo.one()
    |> Repo.preload(:credential)
  end

  def authenticate_by_email_and_pass(email, given_pass) do
    user = get_user_by_email(email)
    cond do
      user && Comeonin.Pbkdf2.checkpw(given_pass, user.credential.password_hash) ->
        {:ok, user}
      user ->
        {:error, :unauthorized}
      true ->
        Comeonin.Pbkdf2.dummy_checkpw()
        {:error, :not_found}
    end
  end
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def get_user(id) do
    Repo.get(User, id)
  end

  def get_user!(id) do
    Repo.get!(User, id)
  end

  def get_user_by(params) do
    Repo.get_by(User, params)
  end

  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  def change_registration(%User{} = user, params) do
    User.registration_changeset(user, params)
  end

  def register_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def list_users do
    Repo.all(User)
  end

end
