module Location exposing (Location, LocationStatus(..), initialLocation, viewLocation)

import Html exposing (..)


type LocationStatus
    = Default
    | Initialized


type alias Location =
    { line1 : String
    , line2 : String
    , city : String
    , state : String
    , postalCode : String
    , country : String
    , status : LocationStatus
    }


initialLocation : Location
initialLocation =
    { line1 = ""
    , line2 = ""
    , city = ""
    , state = ""
    , postalCode = ""
    , country = "USA"
    , status = Default
    }


viewLocation : Location -> Html msg
viewLocation location =
    case location.status of
        Default ->
            text "Not Entered"

        Initialized ->
            text (location.line1 ++ " " ++ location.line2 ++ " " ++ location.city ++ ", " ++ location.state ++ " " ++ location.postalCode)
