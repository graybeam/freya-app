defmodule RsWeb.DivisionTypeController do
  use RsWeb, :controller

  alias Rs.League
  alias Rs.League.DivisionType

  action_fallback RsWeb.FallbackController

  def index(conn, _params) do
    division_types = League.list_division_types()
    render(conn, "index.json", division_types: division_types)
  end

  def create(conn, %{"division_type" => division_type_params}) do
    with {:ok, %DivisionType{} = division_type} <- League.create_division_type(division_type_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.division_type_path(conn, :show, division_type))
      |> render("show.json", division_type: division_type)
    end
  end

  def show(conn, %{"id" => id}) do
    division_type = League.get_division_type!(id)
    render(conn, "show.json", division_type: division_type)
  end

  def update(conn, %{"id" => id, "division_type" => division_type_params}) do
    division_type = League.get_division_type!(id)

    with {:ok, %DivisionType{} = division_type} <- League.update_division_type(division_type, division_type_params) do
      render(conn, "show.json", division_type: division_type)
    end
  end

  def delete(conn, %{"id" => id}) do
    division_type = League.get_division_type!(id)

    with {:ok, %DivisionType{}} <- League.delete_division_type(division_type) do
      send_resp(conn, :no_content, "")
    end
  end
end
