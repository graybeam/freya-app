defmodule RsWeb.Registration.PlayerController do
  use RsWeb, :controller

  alias Rs.Registration
  alias Rs.Registration.Player

  action_fallback RsWeb.FallbackController

  def index(conn, _params) do
    players = Registration.list_players()
    render(conn, "index.json", players: players)
  end

  def create(conn, %{"player" => player_params}) do
    with {:ok, %Player{} = player} <- Registration.create_player(player_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.registration_player_path(conn, :show, player))
      |> render("show.json", player: player)
    end
  end

  def show(conn, %{"id" => id}) do
    player = Registration.get_player!(id)
    render(conn, "show.json", player: player)
  end

  def update(conn, %{"id" => id, "player" => player_params}) do
    player = Registration.get_player!(id)

    with {:ok, %Player{} = player} <- Registration.update_player(player, player_params) do
      render(conn, "show.json", player: player)
    end
  end

  def delete(conn, %{"id" => id}) do
    player = Registration.get_player!(id)

    with {:ok, %Player{}} <- Registration.delete_player(player) do
      send_resp(conn, :no_content, "")
    end
  end
end
