defmodule RsWeb.Repo.Migrations.AddSlugToLeagues do
  use Ecto.Migration

  def change do
    alter table(:leagues) do
      add :slug, :string
    end
  end
end
