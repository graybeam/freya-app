defmodule RsWeb.DivisionTypeControllerTest do
  use RsWeb.ConnCase

  alias Rs.League
  alias Rs.League.DivisionType

  @create_attrs %{
    division: "some division",
    title: "some title"
  }
  @update_attrs %{
    division: "some updated division",
    title: "some updated title"
  }
  @invalid_attrs %{division: nil, title: nil}

  def fixture(:division_type) do
    {:ok, division_type} = League.create_division_type(@create_attrs)
    division_type
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all division_types", %{conn: conn} do
      conn = get(conn, Routes.division_type_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create division_type" do
    test "renders division_type when data is valid", %{conn: conn} do
      conn = post(conn, Routes.division_type_path(conn, :create), division_type: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.division_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "division" => "some division",
               "title" => "some title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.division_type_path(conn, :create), division_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update division_type" do
    setup [:create_division_type]

    test "renders division_type when data is valid", %{conn: conn, division_type: %DivisionType{id: id} = division_type} do
      conn = put(conn, Routes.division_type_path(conn, :update, division_type), division_type: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.division_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "division" => "some updated division",
               "title" => "some updated title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, division_type: division_type} do
      conn = put(conn, Routes.division_type_path(conn, :update, division_type), division_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete division_type" do
    setup [:create_division_type]

    test "deletes chosen division_type", %{conn: conn, division_type: division_type} do
      conn = delete(conn, Routes.division_type_path(conn, :delete, division_type))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.division_type_path(conn, :show, division_type))
      end
    end
  end

  defp create_division_type(_) do
    division_type = fixture(:division_type)
    {:ok, division_type: division_type}
  end
end
