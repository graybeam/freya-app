defmodule Rs.Repo.Migrations.AddTeamsToLeagueRegistrationTable do
  use Ecto.Migration

  def change do
    alter table(:league_registrations) do
      add :team_id, references(:teams, on_delete: :nothing)
    end

    create index(:league_registrations, [:team_id])
  end
end
