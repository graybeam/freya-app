defmodule RsWeb.Schema.DataTypes do
  use Absinthe.Schema.Notation
  alias RsWeb.Resolvers.League

  @desc "A league"
  object :league do
    field :id, :id
    field :name, :string
    field :location, :string
    field :url, :string
    field :slug, :string
    field :contacts, list_of(:contact)
    field :league_registration, :league_registration
  end



  @desc "A contact"
  object :contact do
    field :name, :string
    field :email, :string
    field :phone, :string
    field :role, :string
  end

  @desc "A team"
  object :team do
    field :name, :string
  end

  @desc "A league registration"
  object :league_registration do
    field :url, :string
    #field :teams, list_of(:team)
  end

  @desc "A hash redirect link for a given url"
  object :hashlink do
    field :url, :string
    field :hash, :string
  end


  object :user do
    field :id, :id
    field :username, :string
    field :leagues, list_of(:league) do
      resolve &RsWeb.Resolvers.League.list_leagues/3
    end
  end

end
