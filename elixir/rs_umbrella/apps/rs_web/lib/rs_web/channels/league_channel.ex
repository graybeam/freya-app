defmodule RsWeb.LeagueChannel do
  use RsWeb, :channel

  alias Rs.{Accounts, Organization}
  alias RsWeb.AnnotationView

  def join("leagues:" <> league_id, params, socket) do
    last_seen_id = params["last_seen_id"] || 0
    league_id = String.to_integer(league_id)
    league = Organization.get_league!(league_id)

    annotations =
      league
      |> Organization.list_annotations(last_seen_id)
      |> Phoenix.View.render_many(AnnotationView, "annotation.json")

    {:ok, %{annotations: annotations}, assign(socket, :league_id, league_id)}
  end

  def handle_in(event, params, socket) do
    user = Accounts.get_user!(socket.assigns.user_id)
    handle_in(event, params, user, socket)
  end

  def handle_in("new_annotation", params, user, socket) do
    case Organization.annotate_league(user, socket.assigns.league_id, params) do
      {:ok, annotation} ->
        broadcast!(socket, "new_annotation", %{
          id: annotation.id,
          user: RsWeb.UserView.render("user.json", %{user: user}),
          body: annotation.body,
          at: annotation.at
        })

        {:reply, :ok, socket}

      {:error, changeset} ->
        {:reply, {:error, %{errors: changeset}}, socket}
    end
  end
end
