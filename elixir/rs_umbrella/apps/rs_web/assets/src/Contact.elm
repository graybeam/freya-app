module Contact exposing (Contact)


type alias Contact =
    { id : Int
    , name : String
    , email : String
    , phone : String
    }
