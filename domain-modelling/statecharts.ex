defmodule Rs.League.Statecharts do
  @behaviour :gen_statem
  @name :league_registration_statem

  defstruct [:initial, :states, :history, :current_state]
  alias __MODULE__

  def start_link do
    :gen_statem.start_link({:local, @name}, __MODULE__, [],[])
  end

  def initial_state() do
    %Statecharts{
      initial: :initial,
      states: [
        initial: [on: [register: :registering]],
        registering: [on: [review: :reviewing]],
        reviewing: [on: [submit: :submitting]],
        submitting: [on: [complete: :registered]],
      ],
      history: [],
      current_state: :initial,
    }
  end


  def handle_event(:cast, :flip, :off, data) do
    {:next_state, :on, data + 1}
  end

  def handle_event(:cast, :flip, :on, data) do
    {:next_state, :off, data}
  end

  def handle_event({:call, from}, :get_count, state, data) do
    {:next_state, state, data, [{:reply, from, data}]}
  end

  def init([]) do
    {:ok, :initial, initial_state()}
  end

  def callback_mode do
    :state_functions
  end

  def save_state(state, action) do
    state.history ++ action 
  end

  def apply_action(state, action) do
    current_state = state.current_state


  end

  def update_state(state, action) do
    state
  end
end
